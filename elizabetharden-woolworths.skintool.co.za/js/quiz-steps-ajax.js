function autoHeight(){
    "use strict";
       var mainheader = $('.main-wrapper').find('.header-logo').innerHeight();
        var footer = $('.main-wrapper').find('footer.footer').innerHeight();
       var window_height = $(window).height();
       var hf = mainheader + footer;
	   var fs = hf + 50;
    if ($('.main-wrapper').length) {
        $('.main-wrapper').find('.section-wrapper').css('min-height', window_height);
        $('.main-wrapper').find('.section-wrapper').css('padding-top', mainheader);
        $('.main-wrapper').find('.section-wrapper').css('padding-bottom', footer );
		$('.is-desktop .main-wrapper').find('.final-scroll').css('max-height', window_height - fs);
    }
}
function autoScroll(){
	$(".is-desktop .final-scroll").mCustomScrollbar({
		scrollbarPosition: "outside"
	});
}
function quizImage(){
	$('.quiz-image').Lazy({delay: 200,effect:'fadeIn',onError:function(element){console.log('error loading '+element.data('src'));}});
}
jQuery(window).on("load",function($){
	autoScroll()
});
jQuery(window).resize(function ($) {
	autoScroll()
});
jQuery(document).ready(function($){
	$('body').on('click','#quiz-section-1',function(){
		var step = $(this).data("step");
		$.ajax({
		  type: "post",
		  url: "ajax_quiz-step.php",
		  data: {'step':step},
		  success:function(result){
			$("#quiz-step-section").html(result);
			autoHeight();
			quizImage();
			if ($("body").hasClass("is-mobile")) {
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		  }
		});
		return false;
   	});
	$('body').on('click','#quiz-section-2,#quiz-section-3,#quiz-section-4,#quiz-section-5,#quiz-section-6,#quiz-section-7,#quiz-section-8,#quiz-section-9',function(){
		var selected = $("input[type='radio']:checked");
		if (selected.length > 0) {
			var step = $(this).data("step");
			var quiz = $("input[type='radio']:checked").val();
			var com_ans = $("input[type='radio']:checked").data("ca");
			$.ajax({
				type: "post",
				url: "ajax_quiz-step.php",
				data: {'step':step,'quiz':quiz,'com_ans':com_ans},
				success:function(result){
					$("#quiz-step-section").html(result);
					autoHeight();
					quizImage();
					if ($("body").hasClass("is-mobile")) {
						$('html, body').animate({ scrollTop: 0 }, 'slow');
					}
					if($('.birthdate').length){
						$('.birthdate').datepicker({
							format: 'dd MM',
							weekStart: 1,
							startView: 1,
							changeYear: false,
							maxViewMode: 1,
							autoclose: true,
						}).on('show', function() {
								var dateText  = $(".datepicker-days .datepicker-switch").text().split(" ");
								var dateTitle = dateText[0];
								$(".datepicker-days .datepicker-switch").text(dateTitle);
								$(".datepicker-months .datepicker-switch").css({"visibility":"hidden"});
						});
					}
				}
			 });
         }else {
			swal({
			  title: "Oh no!",
			  text: "It seems like you’ve missed a question.",
			  icon: "error",
			  closeOnClickOutside: false,
			  closeOnEsc: false,
			});
       }
		return false;
   	});
	$('body').on('click','#see-results',function(){
		var valid;	
		valid = validateContact();
		if(valid) {
			var step = $(this).data("step");
			var firstName = $('#firstName').val();
			var lastName = $('#lastName').val();
			var birthDate = $('#birthDate').val();
			var emailAddress = $('#emailAddress').val();
			var mobileNumber = $('#mobileNumber').val();
			var skinEthnicity = $('#skinEthnicity').val();
			var rpm = $('input[type="checkbox"]').prop("checked");			
			if(rpm == true){ var receivePromotionalMaterial = 1; }else{ var receivePromotionalMaterial = 0; }
			var gpm = $('#grantPermission').prop("checked");
			if(gpm == true){ var grantPermission = 1; }else{ var grantPermission = 0; }
			$.ajax({
				type: "post",
				url: "ajax_quiz-step.php",
				data: {'step':step,'firstName':firstName,'lastName':lastName,'birthDate':birthDate,'emailAddress':emailAddress,'mobileNumber':mobileNumber,'skinEthnicity':skinEthnicity,'receivePromotionalMaterial':receivePromotionalMaterial,'grantPermission':grantPermission},
				success:function(result){
					$("#quiz-step-section").html(result);
					autoHeight();
				 	autoScroll();
					quizImage();
					if ($("body").hasClass("is-mobile")) {
						$('html, body').animate({ scrollTop: 0 }, 'slow');
					}
				}
			 });
		}else{
			$("#error-validate-message").text("Oops, please fill in all the fields");
			$("#error-validate-message").css('color','#d31e3d');
			$("#emailAddress").removeClass('error');
		}
		return false;
   	});
	function validateContact() {
		var valid = true;
		if(!$("#firstName").val()) {
			$("#firstName").css('border-color','#d31e3d');
			$("#firstName").addClass('error');
			valid = false;
		}
		if(!$("#lastName").val()) {
			$("#lastName").css('border-color','#d31e3d');
			$("#lastName").addClass('error');
			valid = false;
		}
		if(!$("#birthDate").val()) {
			$("#birthDate").css('border-color','#d31e3d');
			$("#birthDate").addClass('error');
			valid = false;
		}
		if(!$("#mobileNumber").val()) {
			$("#mobileNumber").css('border-color','#d31e3d');
			$("#mobileNumber").addClass('error');
			valid = false;
		}
		if(!$("#emailAddress").val()) {
			$("#emailAddress").css('border-color','#d31e3d');
			$("#emailAddress").addClass('error');
			valid = false;
		}
		if(!$("#emailAddress").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
			$("#emailAddress").css('border-color','#d31e3d');
			$("#emailAddress").css('color','#d31e3d');
			$("#emailAddress").addClass('error');
			valid = false;
		}
		if(!$("#skinEthnicity").val()) {
			$("#skinEthnicity").css('border-color','#d31e3d');
			$("#skinEthnicity").addClass('error');
			valid = false;
		}	
		return valid;
	}
});