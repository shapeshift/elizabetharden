<style type="text/css">
a, a:visited {
	text-decoration:none !important;
}
a[x-apple-data-detectors] {
	color:inherit !important;
	text-decoration:none !important;
	font-size:inherit !important;
	font-weight:inherit !important;
	line-height:inherit !important;
}
@font-face {
	font-family: 'Arial';
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial.woff2') format('woff2'),url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial.woff') format('woff');
	font-weight: 500;
	font-style: normal;
}
@font-face {
	font-family: 'ArialBold';
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial-BoldMT.eot');
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial-BoldMT.eot?#iefix') format('embedded-opentype'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial-BoldMT.woff') format('woff'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial-BoldMT.ttf') format('truetype'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Arial-BoldMT.svg#ArialBold') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'Gotham';
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.eot');
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.eot?#iefix') format('embedded-opentype'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.woff') format('woff'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.ttf') format('truetype'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.svg#Gotham') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'Gotham';
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.eot');
	src: url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.eot?#iefix') format('embedded-opentype'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.woff') format('woff'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.ttf') format('truetype'), url('<?php echo $_SESSION['base_url']; ?>email/fonts/Gotham.svg#Gotham') format('svg');
	font-weight: normal;
	font-style: normal;
}
.space-22 {
	width: 22px !important;
}
</style>
<?php
//$headers  = 'MIME-Version: 1.0' . "\r\n";
//$headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
//$headers .= "From: Elizabeth Arden <info@elizabetharden.co.za>\r\n";
$to = $_SESSION['emailAddress'];
$subject = "Your Elizabeth Arden Skincare Results";
$body = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f2f2f2" role="presentation">';
  $body .= '<tbody>';
    $body .= '<tr>';
      $body .= '<td align="center" style="padding-left:0px;padding-right:0px;padding-top:30px;padding-bottom:30px;"><table width="650" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #d6d6d6;">';
          $body .= '<tbody>';
            $body .= '<tr>';
              $body .= '<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td align="center" valign="top"><img src="http://elizabetharden-foschini.skintool.co.za/images/email-header.jpg" alt="" width="697" height="399" style="display:block;" border="0" hspace="0" vspace="0"></td>
                    </tr>
                  </tbody>
                </table>';
                $body .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;" role="presentation">';
                  $body .= '<tbody>';
                    $body .= '<tr>';
                      $body .= '<td style="color:#0b2039;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 14px;line-height: 20px;margin:0px;padding-bottom:15px;"><font face="Arial">Hi '.$_SESSION['firstName'].' '.$_SESSION['lastName'].',</font></td>';
                    $body .= '</tr>';
                    $body .= '<tr>';
                      $body .= '<td  style="color:#0b2039;font-family: Arial;font-style: normal;font-weight: normal;font-size: 14px;line-height: 20px;margin:0px;padding-bottom:15px;"><font face="Arial">Thank you for using our Skin Diagnostic Tool. Our founder – Elizabeth Arden – believed that, "To achieve beauty, a woman must first achieve health." We created our Skincare Diagnostic Tool with this vision in mind: to ensure that you are recommended the very best skincare products to suit your concerns, enhance the health of your skin and to complement your own natural beauty.</font></td>';
                   $body .= '</tr>';
                    $body .= '<tr>';
                      $body .= '<td style="color:#0b2039;font-family: Arial;font-style: normal;font-weight: normal;font-size: 14px;line-height: 20px;margin:0px;"><font face="Arial">You can learn more about our wide range of beauty products <a href="https://www.elizabetharden.co.za/" style="color:#d31e3d; text-decoration:none !important;outline:none;" target="_blank"><strong style="color:#d31e3d;">here</strong></a> or hit the Shop Now on your results to purchase your ideal products easily and have them delivered straight to your door. Don\'t forget to spread the love and share this amazing tool with your friends!</font></td>';
                    $body .= '</tr>';
                  $body .= '</tbody>';
                $body .= '</table>';
                $body .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#faeaed" style="padding-top:30px;padding-bottom:0px;padding-left:30px;padding-right:30px;" role="presentation">';
                  $body .= '<tbody>';
                    $body .= '<tr>';
                      $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 20px;line-height: 20px;margin:0px;"><font face="Arial">Here are your results</font></td>';
                    $body .= '</tr>';
                  $body .= '</tbody>';
                $body .= '</table>';
                $body .= '<table width="100%" border="0" cellpadding="0" cellspacing="30" role="presentation" bgcolor="#faeaed">';
                  $body .= '<tbody>';
                    $body .= '<tr>';
				   	$cleanser_query = "SELECT * FROM product_data WHERE product_name = '". $cleanser_higher_value_key ."'";
					$cleanser_result = mysqli_query($connection, $cleanser_query) or die(mysqli_error($connection)); 
					while ($cleanser_row = mysqli_fetch_array($cleanser_result, MYSQLI_BOTH)) {
                      $body .= '<td bgcolor="#ffffff" align="center" valign="top"  style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">';
                          $body .= '<tbody>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;"><font face="Arial">Step 1: Cleanse</font></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td><a href="'.$cleanser_row['product_buy_url'].'" style="outline:none;" target="_blank"><img alt="'.$cleanser_row['product_name'].'" src="'.$_SESSION['base_url'].'product-images/'. $cleanser_row['product_image'].'" width="264" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;" align="right"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation" align="right">';
                                  $body .= '<tbody>';
                                    $body .= '<tr>';
                                      $body .= '<td align="left" style="border-bottom:1px solid #000000;">&nbsp;</td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$cleanser_row['product_info_url'].'" style="outline:none;" target="_blank"> <img alt="'.$cleanser_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/info-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$cleanser_row['product_buy_url'].'" style="outline:none;" target="_blank"> <img alt="'.$cleanser_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/shopping-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                    $body .= '</tr>';
                                  $body .= '</tbody>';
                                $body .= '</table></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#000000;font-family: Arial;font-style: normal;font-weight: Bold;font-size:14px;line-height:16px;margin:0px;padding-top:20px;"><a href="'.$cleanser_row['product_buy_url'].'" style="outline:none;font-family: Arial;text-decoration:none;color:#000000;" target="_blank"><font face="Arial">'.$cleanser_row['product_name'].'</font></a></td>';
                            $body .= '</tr>';
                          $body .= '</tbody>';
                        $body .= '</table></td>';
                    }
				   	$universal_product_query = "SELECT * FROM product_data WHERE product_name = '". $universal_product_higher_value_key ."'";
					$universal_product_result = mysqli_query($connection, $universal_product_query) or die(mysqli_error($connection)); 
					while ($universal_product_row = mysqli_fetch_array($universal_product_result, MYSQLI_BOTH)) {
                      $body .= '<td bgcolor="#ffffff" align="center" valign="top"  style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">';
                          $body .= '<tbody>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;"><font face="Arial">Step 2: Prep</font></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td><a href="'.$universal_product_row['product_buy_url'].'" style="outline:none;" target="_blank"><img alt="'.$universal_product_row['product_name'].'" src="'.$_SESSION['base_url'].'product-images/'. $universal_product_row['product_image'].'" width="264" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;" align="right"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation" align="right">';
                                  $body .= '<tbody>';
                                    $body .= '<tr>';
                                      $body .= '<td align="left" style="border-bottom:1px solid #000000;">&nbsp;</td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$universal_product_row['product_info_url'].'" style="outline:none;" target="_blank"> <img alt="'.$universal_product_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/info-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$universal_product_row['product_buy_url'].'" style="outline:none;" target="_blank"> <img alt="'.$universal_product_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/shopping-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                    $body .= '</tr>';
                                  $body .= '</tbody>';
                                $body .= '</table></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#000000;font-family: Arial;font-style: normal;font-weight: Bold;font-size:14px;line-height:16px;margin:0px;padding-top:20px;"><a href="'.$universal_product_row['product_buy_url'].'" style="outline:none;font-family: Arial;text-decoration:none;color:#000000;" target="_blank"><font face="Arial">'.$universal_product_row['product_name'].'</font></a></td>';
                            $body .= '</tr>';
                          $body .= '</tbody>';
                        $body .= '</table></td>';
                    }
                    $body .= '</tr>';
                    $body .= '<tr>';
				   	$treat_query = "SELECT * FROM product_data WHERE product_name = '". $treat_higher_value_key ."'";
					$treat_result = mysqli_query($connection, $treat_query) or die(mysqli_error($connection)); 
					while ($treat_row = mysqli_fetch_array($treat_result, MYSQLI_BOTH)) {
                      $body .= '<td bgcolor="#ffffff" align="center" valign="top"  style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">';
                          $body .= '<tbody>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;"><font face="Arial">Step 3: Treat</font></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td><a href="'.$treat_row['product_buy_url'].'" style="outline:none;" target="_blank"><img alt="'.$treat_row['product_name'].'" src="'.$_SESSION['base_url'].'product-images/'. $treat_row['product_image'].'" width="264" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;" align="right"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation" align="right">';
                                  $body .= '<tbody>';
                                    $body .= '<tr>';
                                      $body .= '<td align="left" style="border-bottom:1px solid #000000;">&nbsp;</td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$treat_row['product_info_url'].'" style="outline:none;" target="_blank"> <img alt="'.$treat_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/info-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$treat_row['product_buy_url'].'" style="outline:none;" target="_blank"> <img alt="'.$treat_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/shopping-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                    $body .= '</tr>';
                                  $body .= '</tbody>';
                                $body .= '</table></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#000000;font-family: Arial;font-style: normal;font-weight: Bold;font-size:14px;line-height:16px;margin:0px;padding-top:20px;"><a href="'.$treat_row['product_buy_url'].'" style="outline:none;font-family: Arial;text-decoration:none;color:#000000;" target="_blank"><font face="Arial">'.$treat_row['product_name'].'</font></a></td>';
                            $body .= '</tr>';
                          $body .= '</tbody>';
                        $body .= '</table></td>';
                    }
				   	$moisturise_query = "SELECT * FROM product_data WHERE product_name = '". $moisturise_higher_value_key ."'";
					$moisturise_result = mysqli_query($connection, $moisturise_query) or die(mysqli_error($connection)); 
					while ($moisturise_row = mysqli_fetch_array($moisturise_result, MYSQLI_BOTH)) {
                      $body .= '<td bgcolor="#ffffff" align="center" valign="top"  style="padding:20px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">';
                          $body .= '<tbody>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;"><font face="Arial">Step 4: Moisturise</font></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td><a href="'.$moisturise_row['product_buy_url'].'" style="outline:none;" target="_blank"><img alt="'.$moisturise_row['product_name'].'" src="'.$_SESSION['base_url'].'product-images/'. $moisturise_row['product_image'].'" width="264" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                           $body .= ' </tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#d31e3d;font-family: Arial;font-style: normal;font-weight: Bold;font-size: 16px;line-height: 18px;margin:0px;" align="right"><table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation" align="right">';
                                  $body .= '<tbody>';
                                    $body .= '<tr>';
                                      $body .= '<td align="left" style="border-bottom:1px solid #000000;">&nbsp;</td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$moisturise_row['product_info_url'].'" style="outline:none;" target="_blank"> <img alt="'.$moisturise_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/info-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                      $body .= '<td align="right" width="40" style="width:40px;"><a href="'.$moisturise_row['product_buy_url'].'" style="outline:none;" target="_blank"> <img alt="'.$moisturise_row['product_name'].'" src="'.$_SESSION['base_url'].'email/images/shopping-img.png" width="40" border="0" hspace="0" vspace="0" style="display:block;outline:none;"></a></td>';
                                    $body .= '</tr>';
                                  $body .= '</tbody>';
                                $body .= '</table></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td style="color:#000000;font-family: Arial;font-style: normal;font-weight: Bold;font-size:14px;line-height:16px;margin:0px;padding-top:20px;"><a href="'.$moisturise_row['product_buy_url'].'" style="outline:none;font-family: Arial;text-decoration:none;color:#000000;" target="_blank"><font face="Arial;">'.$moisturise_row['product_name'].'</font></a></td>';
                            $body .= '</tr>';
                          $body .= '</tbody>';
                        $body .= '</table></td>';
                    }
                    $body .= '</tr>';
                  $body .= '</tbody>';
                $body .= '</table>';
                $body .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">';
                  $body .= '<tbody>';
                    $body .= '<tr>';
					
					
                      $body .= '<td width="50%" valign="top"><a href="https://www.elizabetharden.co.za/" target="_blank"><img src="http://elizabetharden-foschini.skintool.co.za/images/email-bot.jpg" width="697" height="240"></a></td>';
					  
					  
                    $body .= '</tr>';
                  $body .= '</tbody>';
                $body .= '</table>';
                $body .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d31e3d" style="padding:30px;color:#ffffff;" align="center">';
                  $body .= '<tbody>';
                    $body .= '<tr>';
                      $body .= '<td><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">';
                          $body .= '<tbody>';
						  	$body .= '<tr>';
							  $body .= '<td align="center" style="padding-bottom:15px;color:#ffffff;font-family: Gotham;font-style: normal;font-weight: normal;font-size:15px;line-height:16px;"><font face="Gotham">Don\'t forget to share the love...</font></td>';
							$body .= '</tr>';
							$body .= '<tr>';
                            $body .= '<tr>';
                              $body .= '<td align="center" style="padding-bottom:30px"><table border="0" cellspacing="0" cellpadding="0" align="center">';
                                  $body .= '<tbody>';
                                      $body .= '<td style="padding-right:20px"><a href="https://www.facebook.com/elizabethardensouthafrica/" style="outline:none;text-decoration:none;" target="_blank"><img alt="" src="'.$_SESSION['base_url'].'email/images/facebook.png" width="10" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                                      $body .= '<td style="padding-right:20px"><a href="https://twitter.com/EArdenSA" style="outline:none;text-decoration:none;" target="_blank"><img alt="" src="'.$_SESSION['base_url'].'email/images/twitter.png" width="22" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                                      $body .= '<td><a href="https://www.instagram.com/elizabetharden/" style="outline:none;text-decoration:none;" target="_blank"><img alt="" src="'.$_SESSION['base_url'].'email/images/ig-logo-white.png" width="18" border="0" hspace="0" vspace="0" style="display:block;"></a></td>';
                                    $body .= '</tr>';
                                  $body .= '</tbody>';
                                $body .= '</table></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td align="center" style="padding-bottom:30px"><img alt="Elizabeth Arden" src="'.$_SESSION['base_url'].'email/images/logo.png" width="200" border="0" hspace="0" vspace="0" style="display:block;"></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td align="center" style="color:ffffff;font-family: Gotham;font-style: normal;font-weight: normal;font-size:12px;line-height:16px;"><font face="Gotham">© 2021 Elizabeth Arden, Inc. All Rights Reserved.</font></td>';
                            $body .= '</tr>';
                            $body .= '<tr>';
                              $body .= '<td align="center" style="color:ffffff;font-family: Gotham;font-style: normal;font-weight: normal;font-size:12px;line-height:16px;"><font face="Gotham"><a href="http://elizabetharden-woolworths.skintool.co.za/dcs/unsubscribe.php?email='.$to.'" style="text-decoration:none;color:#ffffff;">Unsubscribe</a></font></td>';
                            $body .= '</tr>';
                          $body .= '</tbody>';
                        $body .= '</table></td>';
                    $body .= '</tr>';
                  $body .= '</tbody>';
                $body .= '</table></td>';
            $body .= '</tr>';
          $body .= '</tbody>';
        $body .= '</table></td>';
    $body .= '</tr>';
  $body .= '</tbody>';
$body .= '</table>';
//mail($to,$subject,$body,$headers);
?>
<?php
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
$mail = new PHPMailer(true);
$mail->From = 'info@elizabetharden.co.za';
$mail->FromName = 'Elizabeth Arden';
$mail->CharSet = 'UTF-8';
$mail->addAddress($to);
$mail->isHTML(true); 
$mail->Subject = $subject;
$mail->Body = $body;
if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    //echo 'Message has been sent';
}
?>