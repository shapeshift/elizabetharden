<?php
require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;
if ($detect->isMobile() && !$detect->isTablet()) { 
	$device_class = 'is-mobile';
}else if($detect->isTablet() || $detect->isiPad()){
	$device_class = 'is-tablet';
}else{
	$device_class = 'is-desktop';
}
?>
<?php
session_start();
$http = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'? "https://" : "http://";
$base_url = $http . $_SERVER["SERVER_NAME"] . $_SERVER['REQUEST_URI'];
$_SESSION['base_url'] = $base_url;
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" href="https://skintool.co.za/images/RedDoorIcon.png" />
<link rel="shortcut icon" href="https://skintool.co.za/images/RedDoorIcon.png" />
<link rel="apple-touch-icon-precomposed" href="https://skintool.co.za/images/RedDoorIcon.png" />
<title>Elizabeth Arden Skin Diagnostic Tool</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/fontawesome-all.min.css" />
<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700,800&display=swap" rel="stylesheet">
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link href="css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="js/ie-emulation-modes-warning.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZPVQMYM78E"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-ZPVQMYM78E');
</script>
<body class="<?php echo $device_class; ?>">
<div id="rws-preloader"><span id="rws-loading"></span></div>
<div class="main-wrapper">
  <div id="error-message"></div>
  <div id="quiz-step-section">
  <div class="header-logo"><span class="logo"><img src="images/logo.png" alt=""></span><span class="logo" style="margin-top: -10px;padding-left: 30px;padding-bottom: 20px;"><a href="https://www.woolworths.co.za/cat/Beauty/_/N-1z13rz4Z1z13gfx" target="_blank"><img src="uploads/woolworths-white.png" alt="" style="margin-top: -14px;"></a></span></div>
    <section class="hero">
      <div class="section-wrapper">
        <div class="section-wrapper-inner">
          <div class="page-content">
            <div class="hero-content">
              <h1 class="hero-heading">Skincare <br>diagnostic</h1>
              <p class="hero-desc">Follow our step-by-step questionnaire to help you find a tailored solution for your skin concerns and skin type.</p>
            </div>
            <div class="hero-image">
            	<div class="image-wrapper"><img src="uploads/hero-image.png" alt=""></div>
            </div>
          </div>
        </div>
        <div class="step-link link-white hero-btn"><a href="javascript:void(0)" id="quiz-section-1" data-step="1">Get started</a></div>
        <div class="hero-image-mobile">
           <div class="image-wrapper"><img src="uploads/hero-image.png" alt=""></div>
       </div>
      </div>
    </section>
    <footer class="footer">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
  </div>
</div>
<!--page wrapper--> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/smoothscroll.min.js"></script>
<script defer type='text/javascript' src='js/jquery.lazy.min.js'></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>  
<script type="text/javascript" src="js/quiz-steps-ajax.js"></script> 
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178020173-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-178020173-1');
</script>
</body>
</html>