<!doctype html>
<html>
    <head>
        <title>Accepted Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css"/>
        <style>
            /* Body CSS */
            body {
                background-color: #f0f0f0;
            }
            a{
                color: #0071bc;
            }
            
            
            /* Main Page Header CSS */
            header {
                background-color: #001846;
                padding: 20px;
            }
            header .logo {
                margin-bottom: 100px;
            }
            
            
            
            /* Headline CSS Above Banner Image */
            section .content-wrapper .headline {
                background-color: #c2d837;
                margin-top: -25px;
                padding: 25px;
                text-align: center;
            }
            section .content-wrapper .headline h1 {
                color: #001846;
                font-weight: bold;
                margin: 0;
                padding: 0;
                text-transform: uppercase;
            }
            section .content-wrapper .content {
                background-color: #fff;
                padding-bottom: 40px;
            }
            
            
            
            /* Custom CSS */
            .btn-flat{
                border-radius: 0px;
            }
            span.imp-star {
                color: #f00;
                font-size: 16px;
            }
            label {
                color: #7e7e7e;
                font-size: 14px;
            }
            .form-note {
                color: #000119;
                margin-bottom: 20px;
            }
            .content .form-group input[type="submit"] {
                background-color: #7dcdf2;
                color: #fff;
                height: 57px;
                margin-top: 20px;
                text-transform: uppercase;
                width: 200px;
            }
            .drop_down {
                background-color: #c2d837;
                color: #fff;
                height: 34px;
                margin-top: 0;
                padding-top: 8px;
                position: absolute;
                right: 16px;
                text-align: center;
                width: 34px;
            }
            span.select2 {
                border: 1px solid #ccc;
                clear: both;
                display: block;
                overflow: hidden;
                padding: 6px 6px 6px 10px;
                width: 100%;
            }
            span.select2.select2-container--open {
                border: 1px solid #c2d837;
            }
            span.select2 span {
                display: block;
                overflow: hidden;
                width: 100%;
            }
            span.select2-container {
                width: 52.7% !important;
            }
            .form-group span.select2-container {
                width: 100% !important;
            }
            span.select2-container input {
                display: none;
            }
            span.select2-results ul {
                background-color: #fff;
                border-color: -moz-use-text-color #c2d837 #c2d837;
                border-image: none;
                border-style: none solid solid;
                border-width: medium 1px 1px;
                display: block;
                overflow: hidden;
                padding: 0;
                width: 100%;
              }
            span.select2-results ul li {
                padding: 5px;
                list-style: outside none none;
            }
            span.select2-results ul li:hover {
                background-color: #c2d837 !important;
                color: #fff;
            }
            select#diet {
                display: none;
            }



            
            /* Event Block CSS */
            .event-details {
                background-color: #f0f0f0;
                color: #7e7e7e;
                display: block;
                overflow: hidden;
                padding: 20px;
            }
            .event-details .content-heading {
                color: #001846;
                font-size: 18px;
                font-weight: bold;
                margin: 0 0 20px;
                padding: 0;
                text-transform: uppercase;
            }
            .event-details .wrapper {
                clear: both;
                float: left;
                width: 100%;
            }
            .event-details label.pull-left {
                float: left;
                width: 100px;
            }
            .event-details .address {
                display: block;
                overflow: hidden;
            }
            .event-details button {
                background-color: #7dcdf2;
                border-radius: 0;
                color: #fff;
                float: left;
                font-size: 16px;
                margin: 20px 0;
                padding: 20px;
                text-transform: uppercase;
            }
            .event-details button i.glyphicon-map-marker {
                font-size: 15px;
                margin: 0 5px;
            }
            .event-details .contact-info {
                font-size: 11px;
            }
            
            
            
            /* Headline CSS Below Banner Image */
            section .content-wrapper h2.content-heading {
                color: #001846;
                font-weight: bold;
                margin: 0;
                padding: 40px 0;
                text-align: center;
                text-transform: uppercase;
            }
            
            /* Main Page Footer CSS */
            footer {
                background-color: #fff;
                border-top: 1px solid #e7e7e7;
                padding: 20px;
            }
            footer .footer-links ul li {
                float: left;
                list-style: outside none none;
                margin-right: 5px;
            }
            footer .footer-links ul li:last-child {
                margin-right: 0;
            }
            .footer-links {
                clear: both;
                float: right;
            }
            footer .footer-weblinks {
                clear: both;
                float: right;
            }
            
            
            
            /* Bootstrap Overide */
            .form-control{
                border-radius: 0px;
            }
            .form-control:focus {
                border-color: #c2d837;
                box-shadow: none;
                outline: 0 none;
            }
            
            
            /* Copyright CSS */
            .copyright {
                padding: 40px;
                text-align: center;
            }
            
            
            /* Media Query */
            @media only screen and (max-width: 460px){
                
                .container {
                    padding: 0;
                }
                
                section .content-wrapper .content {
                    padding: 0;
                }
                
                .content div.col-md-4 {
                    padding: 0;
                }
                
                .footer-links {
                    display: block;
                    float: none;
                    overflow: hidden;
                    text-align: center;
                    width: 100%;
                }
                .footer-links ul {
                    margin: 0;
                    padding: 0;
                }
                .footer-weblinks {
                    float: none;
                    text-align: center;
                    width: 100%;
                }
            }
            
            
            /* Thankyou Overlay */
            .overlay {
                background-color: #000;
                display: block;
                height: 100%;
                left: 0;
                overflow: hidden;
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 999;
            }
            .overlay .thankyou_header img {
                float: left;
            }
            .overlay .close {
                background: #fff none repeat scroll 0 0;
                float: right;
                height: 35px;
                opacity: 1;
                padding-top: 7px;
                text-align: center;
                width: 35px;
            }
            .thankyou_header {
                clear: both;
                display: block;
                overflow: hidden;
            }
            .thankyou_message {
                border: 1px solid #c2d837;
                color: #fff;
                height: 159px;
                margin: 12% auto 0;
                padding: 20px;
                text-align: center;
                width: 280px;
            }
            .overlay .thankyou_message h3 {
                color: #c2d837;
                font-weight: bold;
                margin: 0 0 15px;
                text-transform: uppercase;
            }
            .overlay .thankyou_message .message {
                color: #7dcdf2;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="overlay" style="display: none;">
            <div class="thankyou_header">
                <img class="img-responsive" src="bootstrap/img/hi5digital-logo.png" alt="hi5digital"/>
                <div class="close"><span>X</span></div>
            </div>
            <div class="thankyou_message">
                <h3 class="">thank you</h3>
                <div class="message">
                    Your feedback is important to us.
                </div>
                We look forward to welcoming you</br> at our upcoming event.
            </div>
        </div>
        <header>
            <div class="logo">
                <img class="img-responsive" src="bootstrap/img/hi5digital-logo.png" alt="hi5digital"/>
            </div>
        </header>
        <section class="container">
            <div class="content-wrapper">
                <div class="headline">
                    <h1>Cloud Transformation</h1>
                </div>
                <div class="banner-image">
                    <img class="img-responsive" src="Images/SoftwareLicensingImage.jpg" alt="hi5digital-banner"/>
                </div>
                <div class="content col-md-12">
                    <h2 class="content-heading">REGISTRATION</h2>
                    <form action="processor.php" method="post">
                        <div class="col-md-8">
                            <div class="form-note">Please fill in the form below. Required fields are marked <span class="imp-star">*</span></div>
                            <div class="form-group">
                                <label for="name">Name <span class="imp-star">*</span></label>
                                <input class="form-control" placeholder="Your Name" name="name" id="name"/>
                            </div>
                            <div class="form-group">
                                <label for="surname">Surname <span class="imp-star">*</span></label>
                                <input class="form-control" name="surname" placeholder="Your Surname" value="" id="surname"/>
                            </div>
                            <div class="form-group">
                                <label for="company">Company <span class="imp-star">*</span></label>
                                <input class="form-control" name="company" placeholder="Company Name" id="company"/>
                            </div>
                            <div class="form-group">
                                <label for="position">Position</label>
                                <input class="form-control" name="position" placeholder="Your Position" id="position"/>
                            </div>
                            <div class="form-group">
                                <label for="email">Email <span class="imp-star">*</span></label>
                                <input class="form-control" name="email" placeholder="Your Email" id="email" />
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact <span class="imp-star">*</span></label>
                                <input class="form-control" name="contact" placeholder="Contact Number" id="contact"/>
                            </div>
                            <div class="form-group">
                                <label for="diet">Dietary Requirements <span class="imp-star">*</span></label>
                                <div class="drop_down"><i class="glyphicon glyphicon-chevron-down"></i></div>
                                <select class="form-control" name="diet" id="diet">
                                    <option value="none">None</option>
                                    <option value="halaal">Halaal</option>
                                    <option value="kosher">Kosher</option>
                                    <option value="vegetarian">Vegetarian</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="campaignId" id="campaignId" value="<?php echo$_GET['id']; ?>">
                                <input type="submit" value="submit" name="accepted" class="btn btn-lg btn-flat"/>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-4">
                        <div class="event-details">
                            <h3 class="content-heading">EVENT DETAILS</h3>
                            <div class="wrapper">
                                <label class="pull-left">Date</label>
                                <div class="">Tuesday, 20 June 2017</div>
                            </div>
                            <div class="wrapper">
                                <label class="pull-left">Time</label>
                                <div class="">09:00 - 11:30</div>
                            </div>
                            <div class="wrapper">
                                <label class="pull-left">Venue</label>
                                <div class="address">
                                  <p>Century City,<br>
                                    Conference Centre,<br>
                                  Hall B</p>
</div>
                            </div>
                            <button class="btn btn-lg btn-block btn-flat"><i class="glyphicon glyphicon-map-marker"></i><a href="https://www.google.co.za/maps/place/Century+City+Conference+Centre/@-33.8960645,18.5025867,17z/data=!3m1!4b1!4m5!3m4!1s0x1dcc5c112ae309a3:0x2e84748d4b1c112!8m2!3d-33.8960645!4d18.5047754" target="_blank">View Map Direction</a></button>
                            <div class="contact-info">If you have any queries, please contact Jolene de lange on <a href="">021 525 7000</a> or <a href="mailTo:jolened@firsttechnology.co.za">jolened@firsttechnology.co.za</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <img class="img-responsive" src="bootstrap/img/hi5digital-footer-logo.png" alt="hi5digital"/>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="footer-links">
                            <ul>
                                <li>Pioneering.</li>
                                <li>Professional.</li>
                                <li>Partners.</li>
                            </ul>
                        </div>
                        <div class="footer-weblinks">
                            <a href="www.firsttech.co.za" target="blank">www.firsttech.co.za</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright">
            &copy; 2014 First Technology (Pty) Ltd. All rights reserved.
        </div>
        <script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"/></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"/></script>
        <script type="text/javascript" src="plugins/select2/select2.full.min.js"/></script>
        <script type="text/javascript">
            $('input[type="submit"]').on('click', function(e){
                e.preventDefault();
                $.post('processor.php',$('form').serialize(),function(data){
                    if(data.status==true){
                        $('.overlay').show();
                    }
                },"json");
            });
            var $example = $("select#diet").select2();
            $('.drop_down').click(function(){
                $example.select2("open");
            });
            $('.overlay .close').on('click',function(){
                $('input').val('');
                $example.val(null).trigger("change");
                $('.overlay').hide();
            });
        </script>
    </body>
</html>