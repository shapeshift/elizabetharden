<?php
 
session_start();

//connection variables

$host = "dedi59.jnb1.host-h.net";
$database = "skintudxfb_db6";
$user = "skintudxfb_6";
$pass = "dEkZ1xTgdd5z9vKPa948";

//Connect to MySQL using PDO.
$pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass);
 
//Create our SQL query.
$sql = "SELECT `id`,`date`,`first_name`,`last_name`,`birth_date`,`email_address`,`mobile_number`,`birthdays_celebrated`,`skin_ethnicity`,`recommended_product_1`,`recommended_product_2`,`recommended_product_3`,`recommended_product_4`,`receive_promotional_material`,`grant_permission` FROM `skintudxfb_db6`.`user_quizzes` ORDER BY `date` ASC;";
 
//Prepare our SQL query.
$statement = $pdo->prepare($sql);
 
//Executre our SQL query.
$statement->execute();
 
//Fetch all of the rows from our MySQL table.
$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
 
//Get the column names.
$columnNames = array();
if(!empty($rows)){
    //We only need to loop through the first row of our result
    //in order to collate the column names.
    $firstRow = $rows[0];
    foreach($firstRow as $colName => $val){
        $columnNames[] = $colName;
    }
}
 
//Setup the filename that our CSV will have when it is downloaded.
$fileName = 'export-1.csv';
 
//Set the Content-Type and Content-Disposition headers to force the download.
header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="' . $fileName . '"');
 
//Open up a file pointer
$fp = fopen('php://output', 'w');
 
//Start off by writing the column names to the file.
fputcsv($fp, $columnNames);
 
//Then, loop through the rows and write them to the CSV file.
foreach ($rows as $row) {
    fputcsv($fp, $row);
}
 
//Close the file pointer.
fclose($fp);
 
?>