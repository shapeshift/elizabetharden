(function ($) {
    "use strict";
function preLoad() {
    $('#rws-preloader').delay(350).fadeOut('slow', function () {
        $(this).remove();
    });
}
function dateBirthday(){
	$('.birthdate').datepicker({
		format: 'dd MM',
		weekStart: 1,
		startView: 1,
		changeYear: false,
		maxViewMode: 1,
		autoclose: true,
	}).on('show', function() {
			var dateText  = $(".datepicker-days .datepicker-switch").text().split(" ");
			var dateTitle = dateText[0];
			$(".datepicker-days .datepicker-switch").text(dateTitle);
			$(".datepicker-months .datepicker-switch").css({"visibility":"hidden"});
	});
}
function quizImage(){
	$('.quiz-image').Lazy({delay: 200,effect:'fadeIn',onError:function(element){console.log('error loading '+element.data('src'));}});
}
function autoHeight(){
    "use strict";
       var mainheader = $('.main-wrapper').find('.header-logo').innerHeight();
        var footer = $('.main-wrapper').find('footer.footer').innerHeight();
		var heroHeight = $('.main-wrapper').find('.hero-image-mobile').innerHeight();
       var window_height = $(window).height();
        var hf = mainheader + footer;
	   var fs = hf + 50;
    if ($('.main-wrapper').length) {
        $('.main-wrapper').find('.section-wrapper').css('min-height', window_height);
        $('.main-wrapper').find('.section-wrapper').css('padding-top', mainheader);
        $('.main-wrapper').find('.section-wrapper').css('padding-bottom', footer );
		$('.is-tablet .main-wrapper').find('.section-wrapper').css('padding-bottom', heroHeight);
		$('.is-mobile .main-wrapper').find('.section-wrapper').css('padding-bottom', heroHeight + footer);
		$('.is-desktop .main-wrapper').find('.final-scroll').css('max-height', window_height - fs);
    }
}
(function($){
	$(window).on("load",function(){
		$(".is-desktop .final-scroll").mCustomScrollbar({
			scrollbarPosition: "outside"
		});
	});
})(jQuery);
$(document).ready(function () {
	autoHeight(), dateBirthday(), quizImage()
	window.onbeforeunload = function() {
	  return "Data will be lost if you leave the page, are you sure?";
	};
});
$(window).load(function () {
    preLoad()
});
$(window).resize(function () {
    autoHeight()
});
})(jQuery);