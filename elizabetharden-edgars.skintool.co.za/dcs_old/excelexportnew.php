<?php
 
session_start();

//connection variables
$host = "dedi59.jnb1.host-h.net";
$database = "Elizabetharden_db24";
$user = "Eliza_28";
$pass = "3Ym34LbD9Agq43e1vKH6";

try {
    $conn = new PDO("mysql:host=$host;dbname=$database", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);    

    $stmt = $conn->prepare("SELECT `id`,`date`,`first_name`,`last_name`,`email_address`,`mobile_number`,`age_range`,`skin_ethnicity`,`receive_promotional_material`,`favourite_skin_care`,`skin_care`,`birthdays_celebrated`,`skin_feel`,`describe_diet`,`spend_outside`,`savvy_suncare`,`stress_levels` FROM `Elizabetharden_db24`.`user_quizzes` ORDER BY `date` ASC;");

    $stmt->execute();

	$filelocation = '';
	$filename     = 'export-1.csv';
	$file_export  =  $filelocation . $filename;
    $data = fopen($file_export, 'w');

    $csv_fields = array();

$csv_fields[] = 'id';
$csv_fields[] = 'date';
$csv_fields[] = 'first_name';
$csv_fields[] = 'last_name';
$csv_fields[] = 'email_address';
$csv_fields[] = 'mobile_number';
$csv_fields[] = 'age_range';
$csv_fields[] = 'skin_ethnicity';
$csv_fields[] = 'receive_promotional_material';
$csv_fields[] = 'favourite_skin_care';
$csv_fields[] = 'skin_care';
$csv_fields[] = 'birthdays_celebrated';
$csv_fields[] = 'skin_feel';
$csv_fields[] = 'describe_diet';
$csv_fields[] = 'spend_outside';
$csv_fields[] = 'savvy_suncare';
$csv_fields[] = 'stress_levels';

	fputcsv($data, $csv_fields);

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        fputcsv($data, $row);
    }

} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}

header( 'Content-Type: text/csv; charset=utf-8; encoding=UTF-8' );
header("Content-Disposition: attachment;filename=export-1.csv");
header("Pragma: no-cache");
header("Expires: 0");
 
?>