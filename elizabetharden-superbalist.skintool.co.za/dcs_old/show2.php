<?php

// Start the session
session_start();

$_SESSION["sql1"] = "start";
$_SESSION["sql2"] = "start";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Elizabeth Arden data</title>
</head>

<style type="text/css">

body {
    font-family: "Open Sans";
    text-transform: initial;
    font-weight: 400;
    font-style: normal;
    font-size: 14px;	
    background-color: #ffeded;
}
	
	
</style>

<body>


<?php


include('config.php');

//connection variables

$host = "dedi59.jnb1.host-h.net";
$database = "Elizabetharden_db24";
$user = "Eliza_28";
$pass = "3Ym34LbD9Agq43e1vKH6";


try {
    $pdo = new PDO("mysql:host=$host;dbname=$database", $user, $pass);
 
    $sql = 'SELECT * FROM `Elizabetharden_db24`.`user_quizzes` ORDER BY `date` ASC';
 
    $q = $pdo->query($sql);
    $q->setFetchMode(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die("Could not connect to the database $dbname :" . $e->getMessage());
}

?>

<table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td style="padding-left:60px;"><p><img src="https://www.elizabetharden.co.za/images/sitewide/EA-Logo-282x40.svg" alt=""/></p>
      <p>&nbsp;</p></td>
      <td style="padding-left:60px;"><a class="download" href="http://elizabetharden-superbalist.skintool.co.za/dcs/excelexportnew1.php"><img src="Images/button.gif"></a></td>
      <td align="center"><b><a href="http://elizabetharden-superbalist.skintool.co.za/dcs/logout.php"> --> Log Out</a></b></td>
    </tr>
  </tbody>
</table>
<table width="600" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td> </td>
      <td> </td>
    </tr>
  </tbody>
</table>


            <table class="table table-bordered table-condensed" style="padding-left:60px;">
                <thead>
                    <tr>
                        <th style="font-family:calibri; font-size:12px;">Date</th>
                        <th style="font-family:calibri; font-size:12px;">First Name</th>
                        <th style="font-family:calibri; font-size:12px;">Last Name</th>
                        <th style="font-family:calibri; font-size:12px;">Birth Date</th>
                        <th style="font-family:calibri; font-size:12px;">Email</th>
                        <th style="font-family:calibri; font-size:12px;">Number</th>
                        <th style="font-family:calibri; font-size:12px;">Age Range</th>
                        <th style="font-family:calibri; font-size:12px;">Skin Ethnicity</th>
                        <th style="font-family:calibri; font-size:12px;">Prod 1</th>
                        <th style="font-family:calibri; font-size:12px;">Prod 2</th>
                        <th style="font-family:calibri; font-size:12px;">Prod 3</th>
                        <th style="font-family:calibri; font-size:12px;">Prod 4</th>
                        <th style="font-family:calibri; font-size:12px;">Opt-in</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($row = $q->fetch()): ?>
                        <tr>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['date']) ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['first_name']) ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['last_name']); ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['birth_date']); ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['email_address']); ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['mobile_number']); ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['birthdays_celebrated']); ?></td>
                            <td style="font-family:calibri; font-size:11px;"><?php echo htmlspecialchars($row['skin_ethnicity']); ?></td>
                            <td style="font-family:calibri; font-size:10px;"><?php echo htmlspecialchars($row['recommended_product_1']); ?></td>
                            <td style="font-family:calibri; font-size:10px;"><?php echo htmlspecialchars($row['recommended_product_2']); ?></td>
                            <td style="font-family:calibri; font-size:10px;"><?php echo htmlspecialchars($row['recommended_product_3']); ?></td>
                            <td style="font-family:calibri; font-size:10px;"><?php echo htmlspecialchars($row['recommended_product_4']); ?></td>
                            <td style="font-family:calibri; font-size:10px;"><?php echo htmlspecialchars($row['receive_promotional_material']); ?></td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>


</body>
</html>