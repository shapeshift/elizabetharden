<?php

session_start();
session_unset();
session_destroy();
session_write_close();
setcookie(session_name(),'',0,'/');

?>
        
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>First Tech Data</title>
</head>
        
<body bgcolor="#f5f5f5" style="font-family:Tahoma, Geneva, sans-serif; font-size:13px;">
	<div align="center">
    <table width="400" border="0" cellspacing="0" cellpadding="30" bgcolor="#FFFFFF">
  <tr>
    <td><form method="POST" action="show.php"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" align="center"><img src="https://www.elizabetharden.co.za/images/sitewide/EA-Logo-282x40.svg" /></td>
        </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
        </tr>
      <tr>
        <td colspan="2" align="center"><p>Logged out successfully</p>
          <p>Want to Log back in again?</p></td>
        </tr>
      <tr>
        <td>Username:</td>
        <td><input type="text" name="user" /></td>
      </tr>
      <tr>
        <td>Password:</td>
        <td><input type="password" name="pass" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="submit" value="Log in" /></td>
      </tr>
    </table></form></td>
  </tr>
</table>

	</div>
		

</body>
</html>
