<!doctype html>
<html>
    <head>
        <title>Declined</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css"/>
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="plugins/iCheck/all.css">
        <style>
            /* Body CSS */
            body {
                background-color: #f0f0f0;
            }
            a{
                color: #0071bc;
            }
            
            
            /* Main Page Header CSS */
            header {
                background-color: #001846;
                padding: 20px;
            }
            header .logo {
                margin-bottom: 100px;
            }
            
            
            
            /* Headline CSS Above Banner Image */
            section .content-wrapper .headline {
                background-color: #c2d837;
                margin-top: -25px;
                padding: 25px;
                text-align: center;
            }
            section .content-wrapper .headline h1 {
                color: #001846;
                font-weight: bold;
                margin: 0;
                padding: 0;
                text-transform: uppercase;
            }
            section .content-wrapper .content {
                background-color: #fff;
                padding-bottom: 40px !important;
            }
            
            
            
            /* Content Heading */
            .decline-heading {
                text-align: center;
            }
            .content .decline-heading h5 {
                font-weight: bold;
            }

            
            
            /* Headline CSS Below Banner Image */
            section .content-wrapper h2.content-heading {
                color: #001846;
                font-weight: bold;
                margin: 0;
                padding: 40px 0;
                text-align: center;
                text-transform: uppercase;
            }
            
            
            
            /* Custom CSS */
            .btn-flat{
                border-radius: 0px;
            }
            span.imp-star {
                color: #f00;
                font-size: 16px;
            }
            label {
                color: #7e7e7e;
                font-size: 14px;
                font-weight: normal;
            }
            .form-note {
                color: #7e7e7e;
                font-weight: bold;
                margin-bottom: 20px;
                margin-top: 50px;
            }
            .content .form-group input[type="submit"] {
                background-color: #7dcdf2;
                color: #fff;
                height: 57px;
                margin-top: 20px;
                text-transform: uppercase;
                width: 200px;
            }
            textarea.form-control {
                border-color: #898989;
                box-shadow: none;
                height: 160px;
            }
            
            
            /* Main Page Footer CSS */
            footer {
                background-color: #fff;
                border-top: 1px solid #e7e7e7;
                padding: 20px;
            }
            footer .footer-links ul li {
                float: left;
                list-style: outside none none;
                margin-right: 5px;
            }
            footer .footer-links ul li:last-child {
                margin-right: 0;
            }
            .footer-links {
                clear: both;
                float: right;
            }
            footer .footer-weblinks {
                clear: both;
                float: right;
            }
            
            
            
            /* Bootstrap Overide */
            .form-control{
                border-radius: 0px;
            }
            .form-control:focus {
                border-color: #c2d837;
                box-shadow: none;
                outline: 0 none;
            }
            
            
            
            /* Copyright CSS */
            .copyright {
                padding: 40px;
                text-align: center;
            }
            
            
            
            /* Media Query */
            @media only screen and (max-width: 460px){
                
                .container {
                    padding: 0;
                }
                
                section .content-wrapper .content {
                    padding: 0;
                }
                
                .content div.col-md-4 {
                    padding: 0;
                }
                
                .footer-links {
                    display: block;
                    float: none;
                    overflow: hidden;
                    text-align: center;
                    width: 100%;
                }
                .footer-links ul {
                    margin: 0;
                    padding: 0;
                }
                .footer-weblinks {
                    float: none;
                    text-align: center;
                    width: 100%;
                }
                .overlay .thankyou_header img {
                    padding: 10px;
                    width: 65%;
                }
            }
            
            
            /* Thankyou Overlay */
            .overlay {
                background-color: #000;
                display: block;
                height: 100%;
                left: 0;
                overflow: hidden;
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 999;
            }
            .overlay .thankyou_header img {
                float: left;
            }
            .overlay .close {
                background: #fff none repeat scroll 0 0;
                float: right;
                height: 35px;
                opacity: 1;
                padding-top: 7px;
                text-align: center;
                width: 35px;
            }
            .thankyou_header {
                clear: both;
                display: block;
                overflow: hidden;
            }
            .thankyou_message {
                border: 1px solid #c2d837;
                color: #fff;
                height: 159px;
                margin: 12% auto 0;
                padding: 20px;
                text-align: center;
                width: 280px;
            }
            .overlay .thankyou_message h3 {
                color: #c2d837;
                font-weight: bold;
                margin: 0 0 15px;
                text-transform: uppercase;
            }
            .overlay .thankyou_message .message {
                color: #7dcdf2;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="overlay" style="display: none;">
            <div class="thankyou_header">
                <img class="img-responsive" src="bootstrap/img/hi5digital-logo.png" alt="hi5digital"/>
                <div class="close"><span>X</span></div>
            </div>
            <div class="thankyou_message">
                <h3 class="">thank you</h3>
                <div class="message">
                    Your feedback is important to us.
                </div>
                We hope to see you at our</br>future events.
            </div>
        </div>
        <header>
            <div class="logo">
                <img class="img-responsive" src="bootstrap/img/hi5digital-logo.png" alt="hi5digital"/>
            </div>
        </header>
        <section class="container">
            <div class="content-wrapper">
                <div class="headline">
                    <h1><?php 
if($_GET['id']==3){
		echo("Data and the always-on business environment");
	}elseif($_GET['id']==2){
		echo("Pure Thought Leadership Breakfast");
	}elseif($_GET['id']==4){
		echo("Pure Flash Event");
	}elseif($_GET['id']==5){
		echo("Lenovo and First Technology Business Breakfast Connect");
	}elseif($_GET['id']==7){
		echo("ITXedge and First Technology Office 365 Adoption Workshop");
	}elseif($_GET['id']==8){
		echo("Preparing your business for a digital-first future");
	}elseif($_GET['id']==9){
		echo("The Next Network");
	}elseif($_GET['id']==10){
		echo("Cloud Transformation");
	}elseif($_GET['id']==11){
		echo("Fortinet Thought Leadership Breakfast");
	}elseif($_GET['id']==12){
		echo("Always-on availability with Veeam and Azure");
	}elseif($_GET['id']==13){
		echo("Transform IT with Dell EMC");
	}elseif($_GET['id']==14){
		echo("AXIZ - The state of our future");
	}elseif($_GET['id']==15){
		echo("Digital Transformation Event");
	}elseif($_GET['id']==16){
		echo("VMware Thought Leadership Breakfast");
	}elseif($_GET['id']==17){
		echo("Canon Thought Leadership Breakfast");
	}elseif($_GET['id']==18){
		echo("Cyber Security Workshop");
	}else{
		echo("Software Licensing Event");
}
				
			 ?></h1>
                </div>
                <div class="banner-image">
                    <img class="img-responsive" src="Images/SoftwareLicensingImage.jpg" alt="hi5digital-banner"/>
                </div>
                <div class="content col-md-12">
                    <h2 class="content-heading">Event Invitation Declined</h2>
                    <form action="processor2.php" method="post">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="decline-heading">
                                <h5>We are sorry that you won&rsquo;t be attending our event.</h5>
                                <p>Please provide us with feedback in order for us to improve our future events.</p>
                            </div>
                            <div class="form-group">
                                <label for="name">Name <span class="imp-star">*</span></label>
                                <input class="form-control" placeholder="Your Name" name="name" id="name"/>
                            </div>
                            <div class="form-group">
                                <label for="surname">Surname <span class="imp-star">*</span></label>
                                <input class="form-control" name="surname" placeholder="Your Surname" value="" id="surname"/>
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact <span class="imp-star">*</span></label>
                                <input class="form-control" name="contact" placeholder="Contact Number" id="contact"/>
                            </div>
                            <div class="form-note">Reason for not attending</div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="decline-reason" value="scheduling-conflict" class="flat-red">
                                    Scheduling conflict
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="decline-reason" value="not-interested-in-event-topics)" class="flat-red">
                                    Not interested in event topic(s)
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="decline-reason" value="not-interested-in-the-featured-speaker" class="flat-red">
                                    Not interested in the featured speaker(s)
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="decline-reason" value="not-relevant-to-my-profession-or-industry" class="flat-red">
                                    Not relevant to my profession or industry
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="decline-reason" value="other" id="other" class="flat-red">
                                    Other (please specify below)
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Please elaborate on your reason</label>
                                <textarea id="otherReson" name="declined-reason-custom" class="form-control" placeholder="Type Your Message Here" disabled=""></textarea>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="campaignId" id="campaignId" value="<?php echo$_GET['id']; ?>">
                                <input type="hidden" value="<?php echo$_GET['email']; ?>" name="email"/>
                                <input type="submit" value="submit" name="submit" class="btn btn-lg btn-flat"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-responsive" src="bootstrap/img/hi5digital-footer-logo.png" alt="hi5digital"/>
                    </div>
                    <div class="col-md-6">
                        <div class="footer-links">
                            <ul>
                                <li>Pioneering</li>
                                <li>Professional</li>
                                <li>Partners</li>
                            </ul>
                        </div>
                        <div class="footer-weblinks">
                            <a href="www.firsttech.co.za" target="blank">www.firsttech.co.za</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright">
            &copy; 2014 First Technology (Pty) Ltd. All rights reserved.
        </div>
        <!-- jQuery 2.1.4 -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"/></script>
        <!-- iCheck 1.0.1 -->
        <script type="text/javascript" src="plugins/iCheck/icheck.min.js"></script>
        <!-- Page script -->
        <script>
            $('input[type="submit"]').on('click', function(e){
                e.preventDefault();
                $.post('processor2.php',$('form').serialize(),function(data){
                    if(data.status==true){
                        $('.overlay').show();
                    }
                },"json");
            });
            $('.overlay .close').on('click',function(){
                $('input').iCheck('uncheck');
                $('textarea').val('');
                $('.overlay').hide();
            });
            $('input').on('ifChecked', function(event){
                $('textarea#otherReson').attr('disabled','disabled');
            });
            $('input#other').on('ifChecked', function(event){
                $('textarea#otherReson').removeAttr('disabled');
            });
            $(function () {
                //iCheck for checkbox and radio inputs
                $('input[type="radio"].minimal').iCheck({
                  checkboxClass: 'icheckbox_minimal-blue',
                  radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="radio"].minimal-red').iCheck({
                  checkboxClass: 'icheckbox_minimal-red',
                  radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="radio"].flat-red').iCheck({
                  checkboxClass: 'icheckbox_flat-green',
                  radioClass: 'iradio_flat-green'
                });
            });
        </script>
    </body>
</html>