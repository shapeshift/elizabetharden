<?php
include 'db_connection.php';
session_start();
if(!empty($_POST["step"]) && $_POST['step'] == 1){
$_SESSION['step1'] = $_POST['step'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step1.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">01</span>
                <h3 class="heading">Which is your preferred Elizabeth Arden skincare product range?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="favourite-skin-care" type="radio" value="Prevage" data-ca="A">
                  <span class="option">Prevage</span>
                  </input>
                </label>
                <label>
                  <input name="favourite-skin-care" type="radio" value="Eight Hour" data-ca="B">
                  <span class="option">Eight Hour</span>
                  </input>
                </label>
                <label>
                  <input name="favourite-skin-care" type="radio" value="Skin Illuminating" data-ca="C">
                  <span class="option">Skin Illuminating</span>
                  </input>
                </label>
                <label>
                  <input name="favourite-skin-care" type="radio" value="Ceramide" data-ca="D">
                  <span class="option">Ceramide</span>
                  </input>
                </label>
                <label>
                  <input name="favourite-skin-care" type="radio" value="Other" data-ca="E">
                  <span class="option">Other</span>
                  </input>
                </label>
                <label>
                  <input name="favourite-skin-care" type="radio" value="I do not currently use Elizabeth Arden products" data-ca="F">
                  <span class="option">I do not currently use Elizabeth Arden products</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-2" data-step="2">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>
 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 2){
$_SESSION['step2'] = $_POST['step'];
$_SESSION['quiz1'] = $_POST['quiz'];
$_SESSION['com_ans1'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step2.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">02</span>
                <h3 class="heading">What are the primary results you want from your skincare regimen?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="skin-care" type="radio" value="Lift & firm" data-ca="A">
                  <span class="option">Lift & firm</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Fight off environmental threats" data-ca="B">
                  <span class="option">Fight off environmental threats</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Moisturise & hydrate" data-ca="C">
                  <span class="option">Moisturise & hydrate</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Soothe & calm" data-ca="D">
                  <span class="option">Soothe & calm</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Reduce redness" data-ca="E">
                  <span class="option">Reduce redness</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Minimise dark spots/skin discolouration" data-ca="F">
                  <span class="option">Minimise dark spots/skin discolouration</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Reverse sun damage" data-ca="G">
                  <span class="option">Reverse sun damage</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Control oil & shine" data-ca="H">
                  <span class="option">Control oil & shine</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Minimise lines & wrinkles" data-ca="I">
                  <span class="option">Minimise lines & wrinkles</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Everyday healthy skin" data-ca="J">
                  <span class="option">Everyday healthy skin</span>
                  </input>
                </label>
                <label>
                  <input name="skin-care" type="radio" value="Control multiple signs of aging" data-ca="K">
                  <span class="option">Control multiple signs of aging</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-3" data-step="3">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>

 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 3){
$_SESSION['step3'] = $_POST['step'];
$_SESSION['quiz2'] = $_POST['quiz'];
$_SESSION['com_ans2'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step3.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">03</span>
                <h3 class="heading">How many birthdays have you celebrated?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="birthdays-celebrated" type="radio" value="24 or under" data-ca="A">
                  <span class="option">24 or under</span>
                  </input>
                </label>
                <label>
                  <input name="birthdays-celebrated" type="radio" value="25-34" data-ca="B">
                  <span class="option">25-34</span>
                  </input>
                </label>
                <label>
                  <input name="birthdays-celebrated" type="radio" value="35-44" data-ca="C">
                  <span class="option">35-44</span>
                  </input>
                </label>
                <label>
                  <input name="birthdays-celebrated" type="radio" value="45-54" data-ca="D">
                  <span class="option">45-54</span>
                  </input>
                </label>
                <label>
                  <input name="birthdays-celebrated" type="radio" value="55+" data-ca="E">
                  <span class="option">55+</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-4" data-step="4">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>

 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 4){
$_SESSION['step4'] = $_POST['step'];
$_SESSION['quiz3'] = $_POST['quiz'];
$_SESSION['com_ans3'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step4.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">04</span>
                <h3 class="heading">How does your skin feel by the middle of the day?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="skin-feel" type="radio" value="Extremely dry & tight" data-ca="A">
                  <span class="option">Extremely dry & tight</span>
                  </input>
                </label>
                <label>
                  <input name="skin-feel" type="radio" value="My cheeks feel a bit dry, but my T-zone is oily/shiny" data-ca="B">
                  <span class="option">My cheeks feel a bit dry, but my T-zone is oily/shiny</span>
                  </input>
                </label>
                <label>
                  <input name="skin-feel" type="radio" value="My cheeks feel normal and comfortable, but my T-zone is oily/shiny" data-ca="C">
                  <span class="option">My cheeks feel normal and comfortable, but my T-zone is oily/shiny</span>
                  </input>
                </label>
                <label>
                  <input name="skin-feel" type="radio" value="Oily/shiny all over" data-ca="D">
                  <span class="option">Oily/shiny all over</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-5" data-step="5">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>

 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 5){
$_SESSION['step5'] = $_POST['step'];
$_SESSION['quiz4'] = $_POST['quiz'];
$_SESSION['com_ans4'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step5.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">05</span>
                <h3 class="heading">How would you describe your diet?</h3>
              </div>
              <div class="options">
              	<label>
                  <input name="describe-diet" type="radio" value="Not healthy at all" data-ca="A">
                  <span class="option">Not healthy at all</span>
                  </input>
                </label>
                <label>
                  <input name="describe-diet" type="radio" value="Fairly healthy" data-ca="B">
                  <span class="option">Fairly healthy</span>
                  </input>
                </label>
                <label>
                  <input name="describe-diet" type="radio" value="Extremely Healthy, I follow a balanced eating plan" data-ca="C">
                  <span class="option">Extremely healthy, I follow a balanced eating plan</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-6" data-step="6">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>

 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 6){
$_SESSION['step6'] = $_POST['step'];
$_SESSION['quiz5'] = $_POST['quiz'];
$_SESSION['com_ans5'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step6.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">06</span>
                <h3 class="heading">In an average week, how much time do you spend outside?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="spend-outside" type="radio" value="Less than an hour" data-ca="A">
                  <span class="option">Less than an hour</span>
                  </input>
                </label>
                <label>
                  <input name="spend-outside" type="radio" value="1-3 hours" data-ca="B">
                  <span class="option">1-3 hours</span>
                  </input>
                </label>
                <label>
                  <input name="spend-outside" type="radio" value="3-5 hours" data-ca="C">
                  <span class="option">3-5 hours</span>
                  </input>
                </label>
                <label>
                  <input name="spend-outside" type="radio" value="More than 5 hours" data-ca="D">
                  <span class="option">More than 5 hours</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-7" data-step="7">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>

 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 7){
$_SESSION['step7'] = $_POST['step'];
$_SESSION['quiz6'] = $_POST['quiz'];
$_SESSION['com_ans6'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step7.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">07</span>
                <h3 class="heading">Are you savvy about suncare?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="savvy-suncare" type="radio" value="Very! I wear SPF 30 or stronger every day" data-ca="A">
                  <span class="option">Very! I wear SPF 30 or stronger every day</span>
                  </input>
                </label>
                <label>
                  <input name="savvy-suncare" type="radio" value="I use a face product that contains SPF 15 daily" data-ca="B">
                  <span class="option">I use a face product that contains SPF 15 daily</span>
                  </input>
                </label>
                <label>
                  <input name="savvy-suncare" type="radio" value="I wear an SPF on my skin during summer months" data-ca="C">
                  <span class="option">I wear an SPF on my skin during summer months</span>
                  </input>
                </label>
                <label>
                  <input name="savvy-suncare" type="radio" value="I only wear an SPF when I go to the beach" data-ca="D">
                  <span class="option">I only wear an SPF when I go to the beach</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-8" data-step="8">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>
 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 8){
$_SESSION['step6'] = $_POST['step'];
$_SESSION['quiz7'] = $_POST['quiz'];
$_SESSION['com_ans7'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo.png" alt="Elizabeth Arden"></a></div>
<section class="block-section">
  <div class="section-wrapper">
    <div class="section-wrapper-inner">
      <div class="main-content">
        <div class="page-content">
          <div class="main-block">
            <div class="block-image"> <img class="quiz-image" data-src="uploads/step8.jpg" alt=""> </div>
            <div class="block-content">
              <div class="block-heading"> <span class="block-num">08</span>
                <h3 class="heading">How would you describe your stress levels?</h3>
              </div>
              <div class="options">
                <label>
                  <input name="stress-levels" type="radio" value="Not stressed much" data-ca="A">
                  <span class="option">Not stressed much</span>
                  </input>
                </label>
                <label>
                  <input name="stress-levels" type="radio" value="Medium stress levels" data-ca="B">
                  <span class="option">Medium stress levels</span>
                  </input>
                </label>
                <label>
                  <input name="stress-levels" type="radio" value="Very stressed, but manageable" data-ca="C">
                  <span class="option">Very stressed, but manageable</span>
                  </input>
                </label>
                <label>
                  <input name="stress-levels" type="radio" value="Unbearably stressed" data-ca="D">
                  <span class="option">Unbearably stressed</span>
                  </input>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="step-link "><a href="javascript:void(0)" id="quiz-section-9" data-step="9">Next</a></div>
  </div>
  <div class="footer-image"><img class="footer-img" src="uploads/footer-img.jpg" alt=""></div>
</section>
 <footer class="footer style-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 9){
$_SESSION['step9'] = $_POST['step'];
$_SESSION['quiz8'] = $_POST['quiz'];
$_SESSION['com_ans8'] = $_POST['com_ans'];
?>
<div class="header-logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo-b.png" alt="Elizabeth Arden"></a></div>
<section class="block-section final-result">
    <div class="section-wrapper">
      <div class="section-wrapper-inner">
        <div class="main-content">
          <div class="page-content">
            <div class="main-block final-block item-center">
              <div class="final-content">
                <div class="block-heading">
                  <h3 class="heading fancy-heading">You’re <br /> almost done!</h3>
               </div>
               <p>Please fill in your details to receive your results via email.</p>
              </div>
              <div class="form-submit">
                <div class="contact-form">
                <h5 class="c-heading">Fill in your details</h5>
                <div id="error-validate-message"></div>
                    <form>
                        <p><input type="text" placeholder="First Name" id="firstName" name="first-name" value="" class="form-control" required></p>
                        <p><input type="text" placeholder="Last Name" id="lastName" name="last-name" value="" class="form-control" required></p>
                        <p><input placeholder="Birth date"  class="birthdate form-control" id="birthDate" name="birth-date" value=""></p>
                        <p><input type="email" placeholder="Email" id="emailAddress" name="email-address" value="" class="form-control" required></p>
                        <p><input type="text" placeholder="Mobile number" id="mobileNumber" name="mobile-number" value="" class="form-control" required></p>
                        <p>
                        <select name="skin-ethnicity" id="skinEthnicity" class="form-control">
                        	<option value="">Skin Ethnicity</option>
                            <option value="Black">Black</option>
                            <option value="Coloured">Coloured</option>
                            <option value="Caucasian">Caucasian</option>
                            <option value="Indian-Asian/Other">Indian-Asian/Other</option>
                        </select>
                        </p>
                        <p class="checkbox">
                            <label><input type="checkbox" value="1" id="receivePromotionalMaterial" name="receive-promotional-material" checked>I would like to receive promotional material</label>
                            <label><input type="checkbox" value="1" id="grantPermission" name="grant-permission">I grant permission for my personal information to be stored</label>
                        </p>
                    </form> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="step-link link-white"><a href="javascript:void(0)" id="see-results" data-step="10" >See results</a></div>
    </div>
</section>
 <footer class="footer color-1">
      <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
  </footer>
<?php } ?>
<?php
$com_ans_que2 = array();
$query2 = "SELECT * FROM products WHERE question = 'skin_care'";
$result2 = mysqli_query($connection, $query2) or die(mysqli_error($connection));
while ($row2 = mysqli_fetch_array($result2, MYSQLI_BOTH)) {
	$com_ans2 = str_replace(',', ' ', $row2['combination_answers']);
	$sel_ans2 = $_SESSION['com_ans2'];
	$pos2 = strpos($com_ans2, $sel_ans2);
	if($pos2 !== false) {
		$cat_name2 = $row2['category_name'];
		$com_ans_que2[$cat_name2][] = $row2['product_name'];
	}
}
$query3 = "SELECT * FROM products WHERE question = 'birthdays_celebrated'";
$result3 = mysqli_query($connection, $query3) or die(mysqli_error($connection));
while ($row3 = mysqli_fetch_array($result3, MYSQLI_BOTH)) {
	$com_ans3 = str_replace(',', ' ', $row3['combination_answers']);
	$sel_ans3 = $_SESSION['com_ans3'];
	$pos3 = strpos($com_ans3, $sel_ans3);
	if($pos3 !== false) {
		$cat_name3 = $row3['category_name'];
		$com_ans_que2[$cat_name3][] = $row3['product_name'];
	}
}
$query4 = "SELECT * FROM products WHERE question = 'skin_feel'";
$result4 = mysqli_query($connection, $query4) or die(mysqli_error($connection));
while ($row4 = mysqli_fetch_array($result4, MYSQLI_BOTH)) {
	$com_ans4 = str_replace(',', ' ', $row4['combination_answers']);
	$sel_ans4 = $_SESSION['com_ans4'];
	$pos4 = strpos($com_ans4, $sel_ans4);
	if($pos4 !== false) {
		$cat_name4 = $row4['category_name'];
		$com_ans_que2[$cat_name4][] = $row4['product_name'];
	}
}
$query5 = "SELECT * FROM products WHERE question = 'describe_diet'";
$result5 = mysqli_query($connection, $query5) or die(mysqli_error($connection));
while ($row5 = mysqli_fetch_array($result5, MYSQLI_BOTH)) {
	$com_ans5 = str_replace(',', ' ', $row5['combination_answers']);
	$sel_ans5 = $_SESSION['com_ans5'];
	$pos5 = strpos($com_ans5, $sel_ans5);
	if($pos5 !== false) {
		$cat_name5 = $row5['category_name'];
		$com_ans_que2[$cat_name5][] = $row5['product_name'];
	}
}
$query6 = "SELECT * FROM products WHERE question = 'spend_outside'";
$result6 = mysqli_query($connection, $query6) or die(mysqli_error($connection));
while ($row6 = mysqli_fetch_array($result6, MYSQLI_BOTH)) {
	$com_ans6 = str_replace(',', ' ', $row6['combination_answers']);
	$sel_ans6 = $_SESSION['com_ans6'];
	$pos6 = strpos($com_ans6, $sel_ans6);
	if($pos6 !== false) {
		$cat_name6 = $row6['category_name'];
		$com_ans_que2[$cat_name6][] = $row6['product_name'];
	}
}
$query7 = "SELECT * FROM products WHERE question = 'savvy_suncare'";
$result7 = mysqli_query($connection, $query7) or die(mysqli_error($connection));
while ($row7 = mysqli_fetch_array($result7, MYSQLI_BOTH)) {
	$com_ans7 = str_replace(',', ' ', $row7['combination_answers']);
	$sel_ans7 = $_SESSION['com_ans7'];
	$pos7 = strpos($com_ans7, $sel_ans7);
	if($pos7 !== false) {
		$cat_name7 = $row7['category_name'];
		$com_ans_que2[$cat_name7][] = $row7['product_name'];
	}
}
$query8 = "SELECT * FROM products WHERE question = 'stress_levels'";
$result8 = mysqli_query($connection, $query8) or die(mysqli_error($connection));
while ($row8 = mysqli_fetch_array($result8, MYSQLI_BOTH)) {
	$com_ans8 = str_replace(',', ' ', $row8['combination_answers']);
	$sel_ans8 = $_SESSION['com_ans8'];
	$pos8 = strpos($com_ans8, $sel_ans8);
	if($pos8 !== false) {
		$cat_name8 = $row8['category_name'];
		$com_ans_que2[$cat_name8][] = $row8['product_name'];
	}
}
if(!empty($com_ans_que2['CLEANSER'])){
	$cleanser = array_count_values($com_ans_que2['CLEANSER']);
	$cleanser_higher_count_value = max($cleanser);
	$cleanser_higher_value_data = array_keys($cleanser,max($cleanser));
	$cleanser_higher_value_key = $cleanser_higher_value_data[0];
}
if(!empty($com_ans_que2['UNIVERSAL PRODUCT'])){
	$universal_product = array_count_values($com_ans_que2['UNIVERSAL PRODUCT']);
	$universal_product_higher_count_value = max($universal_product);
	$universal_product_higher_value_data = array_keys($universal_product,max($universal_product));
	$universal_product_higher_value_key = $universal_product_higher_value_data[0];
}
if(!empty($com_ans_que2['TREAT'])){
	$treat = array_count_values($com_ans_que2['TREAT']);
	$treat_higher_count_value = max($treat);
	$treat_higher_value_data = array_keys($treat,max($treat));
	$treat_higher_value_key = $treat_higher_value_data[0];
}
if(!empty($com_ans_que2['MOISTURISE'])){
	$moisturise = array_count_values($com_ans_que2['MOISTURISE']);
	$moisturise_higher_count_value = max($moisturise);
	$moisturise_higher_value_data = array_keys($moisturise,max($moisturise));
	$moisturise_higher_value_key = $moisturise_higher_value_data[0];
}
?>
<?php
if(!empty($_POST["step"]) && $_POST['step'] == 10){	
$cleanser_query = "SELECT * FROM product_data WHERE product_name = '". $cleanser_higher_value_key ."'";
$cleanser_result = mysqli_query($connection, $cleanser_query) or die(mysqli_error($connection)); 
while ($cleanser_row = mysqli_fetch_array($cleanser_result, MYSQLI_BOTH)) {
	$_SESSION['recommended_product_1'] = $cleanser_row['product_name'];
}	
$universal_product_query = "SELECT * FROM product_data WHERE product_name = '". $universal_product_higher_value_key ."'";
$universal_product_result = mysqli_query($connection, $universal_product_query) or die(mysqli_error($connection)); 
while ($universal_product_row = mysqli_fetch_array($universal_product_result, MYSQLI_BOTH)) {
	$_SESSION['recommended_product_2'] = $universal_product_row['product_name'];
}
$treat_query = "SELECT * FROM product_data WHERE product_name = '". $treat_higher_value_key ."'";
$treat_result = mysqli_query($connection, $treat_query) or die(mysqli_error($connection)); 
while ($treat_row = mysqli_fetch_array($treat_result, MYSQLI_BOTH)) {
	$_SESSION['recommended_product_3'] = $treat_row['product_name'];
}
$moisturise_query = "SELECT * FROM product_data WHERE product_name = '". $moisturise_higher_value_key ."'";
$moisturise_result = mysqli_query($connection, $moisturise_query) or die(mysqli_error($connection)); 
while ($moisturise_row = mysqli_fetch_array($moisturise_result, MYSQLI_BOTH)) {
	$_SESSION['recommended_product_4'] = $moisturise_row['product_name'];
}
$_SESSION['step10'] = $_POST['step'];
$_SESSION['firstName'] = $_POST['firstName'];
$_SESSION['lastName'] = $_POST['lastName'];
$_SESSION['birthDate'] = $_POST['birthDate'];
$_SESSION['emailAddress'] = $_POST['emailAddress'];
$_SESSION['mobileNumber'] = $_POST['mobileNumber'];
$_SESSION['skinEthnicity'] = $_POST['skinEthnicity'];
if($_POST['receivePromotionalMaterial'] == 1){ $rpm = $_POST['receivePromotionalMaterial']; }else{ $rpm = 0; }
if($_POST['grantPermission'] == 1){ $gpm = $_POST['grantPermission']; }else{ $gpm = 0; }
$_SESSION['identityNumber'] = mt_rand();
if(isset($_SESSION['quiz1'])){ $quiz1 = mysqli_real_escape_string($connection, $_SESSION['quiz1']); }
if(isset($_SESSION['quiz2'])){ $quiz2 = mysqli_real_escape_string($connection, $_SESSION['quiz2']); }
if(isset($_SESSION['quiz3'])){ $quiz3 = mysqli_real_escape_string($connection, $_SESSION['quiz3']); }
if(isset($_SESSION['quiz4'])){ $quiz4 = mysqli_real_escape_string($connection, $_SESSION['quiz4']); }
if(isset($_SESSION['quiz5'])){ $quiz5 = mysqli_real_escape_string($connection, $_SESSION['quiz5']); }
if(isset($_SESSION['quiz6'])){ $quiz6 = mysqli_real_escape_string($connection, $_SESSION['quiz6']); }
if(isset($_SESSION['quiz7'])){ $quiz7 = mysqli_real_escape_string($connection, $_SESSION['quiz7']); }
if(isset($_SESSION['quiz8'])){ $quiz8 = mysqli_real_escape_string($connection, $_SESSION['quiz8']); }
if(isset($_SESSION['firstName'])){ $first_name = mysqli_real_escape_string($connection, $_SESSION['firstName']); }
if(isset($_SESSION['lastName'])){ $last_name = mysqli_real_escape_string($connection, $_SESSION['lastName']); }
if(isset($_SESSION['birthDate'])){ $birth_date = mysqli_real_escape_string($connection, $_SESSION['birthDate']); }
if(isset($_SESSION['emailAddress'])){ $email_address = mysqli_real_escape_string($connection, $_SESSION['emailAddress']); }
if(isset($_SESSION['mobileNumber'])){ $mobile_number = mysqli_real_escape_string($connection, $_SESSION['mobileNumber']); }
if(isset($_SESSION['skinEthnicity'])){ $skin_ethnicity = mysqli_real_escape_string($connection, $_SESSION['skinEthnicity']); }
if(isset($_POST['receivePromotionalMaterial'])){ $receive_promotional_material = mysqli_real_escape_string($connection, $rpm); }
if(isset($_POST['grantPermission'])){ $grant_permission = mysqli_real_escape_string($connection, $gpm); }
if(isset($_SESSION['identityNumber'])){ $identity_number = mysqli_real_escape_string($connection, $_SESSION['identityNumber']); }
if(isset($_SESSION['recommended_product_1'])){ $recommended_product_1 = mysqli_real_escape_string($connection, $_SESSION['recommended_product_1']); }
if(isset($_SESSION['recommended_product_2'])){ $recommended_product_2 = mysqli_real_escape_string($connection, $_SESSION['recommended_product_2']); }
if(isset($_SESSION['recommended_product_3'])){ $recommended_product_3 = mysqli_real_escape_string($connection, $_SESSION['recommended_product_3']); }
if(isset($_SESSION['recommended_product_4'])){ $recommended_product_4 = mysqli_real_escape_string($connection, $_SESSION['recommended_product_4']); }
$sql = "INSERT INTO user_quizzes(identity_number,favourite_skin_care,skin_care,birthdays_celebrated,skin_feel,describe_diet,spend_outside,savvy_suncare,stress_levels,first_name,last_name,birth_date,email_address,mobile_number,skin_ethnicity,receive_promotional_material,grant_permission,date,recommended_product_1,recommended_product_2,recommended_product_3,recommended_product_4) VALUES ('$identity_number','$quiz1','$quiz2','$quiz3','$quiz4','$quiz5','$quiz6','$quiz7','$quiz8','$first_name','$last_name','$birth_date','$email_address','$mobile_number','$skin_ethnicity','$receive_promotional_material','$grant_permission',now(),'$recommended_product_1','$recommended_product_2','$recommended_product_3','$recommended_product_4')";
if(mysqli_query($connection, $sql)){
	//echo "Records added successfully.";
} else{
	echo "ERROR: Could not able to execute $sql. " . mysqli_error($connection);
}
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="header-logo"><span class="logo"><a href="<?php echo $_SESSION['base_url']; ?>"><img src="images/logo-b.png" alt="Elizabeth Arden"></a></span><span class="logo" style="margin-top: -10px;padding-left: 30px;padding-bottom: 20px;"><a href="https://www.foschiniforbeauty.co.za/blp/elizabeth-arden/_/N-1z13yk0" target="_blank"><img src="uploads/foschini-black.png" alt="" style="margin-top: -14px;"></a></span></div>
<section class="block-section final-result">
    <div class="section-wrapper">
      <div class="section-wrapper-inner">
        <div class="main-content">
          <div class="page-content">
            <div class="main-block final-products">
              <div class="final-content">
                <div class="block-heading">
                  <h3 class="heading fancy-heading">Here are <br />your results</h3>
               </div>
               <p>Thank you for using our Skin Diagnostic Tool. Here are the products best suited for your skin and concerns.</p>
               <div class="final-info">
               		<div class="info-image">
                    	<img src="uploads/final-image.png" alt="">
                    </div>
                    <div class="info-text">
                    	<p class="m-0">We offer a range of luxurious, high performing products that leverage over 100 years of hands-on skincare expertise. We use this knowledge to ensure that we have all skin types covered.</p>
                    </div>
               </div>
              </div>
              <div class="final-result">
              	<div class="final-scroll">
              <div class="product-grid">
                <div class="product">
                	<?php
				   	$cleanser_query = "SELECT * FROM product_data WHERE product_name = '". $cleanser_higher_value_key ."'";
					$cleanser_result = mysqli_query($connection, $cleanser_query) or die(mysqli_error($connection)); 
					while ($cleanser_row = mysqli_fetch_array($cleanser_result, MYSQLI_BOTH)) { ?>
						<div class="product-image">
                        	<span class="product-step">Step 1: Cleanse</span>
                            <div class="product-share">
                                
                                <span class="icons">
                                    <a href="https://www.facebook.com/elizabethardensouthafrica/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                     <a href="https://twitter.com/EArdenSA" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.instagram.com/elizabetharden/" target="_blank"><i class="fab fa-instagram"></i></a>
                                </span>
                            </div>
                            <img class="quiz-image" data-src="<?php echo $_SESSION['base_url'].'product-images/'. $cleanser_row['product_image']. ''; ?>" alt="<?php echo $cleanser_row['product_name']; ?>">
                            <div class="product-info">
                            
							<?php $r=$cleanser_row['product_info_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $cleanser_row['product_info_url']; ?>" target="_blank"><img src="images/info.svg" alt=""/></a></span>
                            <?php } ?>
                                
							<?php $r=$cleanser_row['product_buy_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $cleanser_row['product_buy_url']; ?>" target="_blank"><img src="images/cart.svg" alt=""/></a></span>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="product-desc">
                            <h4 class="product-title"><?php echo $cleanser_row['product_name']; ?></h4>
                            
                        </div>
                    <?php } ?>
                </div>
                <div class="product">
                	<?php
				   	$universal_product_query = "SELECT * FROM product_data WHERE product_name = '". $universal_product_higher_value_key ."'";
					$universal_product_result = mysqli_query($connection, $universal_product_query) or die(mysqli_error($connection)); 
					while ($universal_product_row = mysqli_fetch_array($universal_product_result, MYSQLI_BOTH)) { ?>
                        <div class="product-image">
                        	<span class="product-step">Step 2: Prep</span>
                            <div class="product-share">
                                
                                <span class="icons">
                                    <a href="https://www.facebook.com/elizabethardensouthafrica/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                     <a href="https://twitter.com/EArdenSA" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.instagram.com/elizabetharden/" target="_blank"><i class="fab fa-instagram"></i></a>
                                </span>
                            </div>
                            <img class="quiz-image" data-src="<?php echo $_SESSION['base_url'].'product-images/'. $universal_product_row['product_image']. ''; ?>" alt="<?php echo $universal_product_row['product_name']; ?>">
                            <div class="product-info">
                            
							<?php $r=$universal_product_row['product_info_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $universal_product_row['product_info_url']; ?>" target="_blank"><img src="images/info.svg" alt=""/></a></span>
                            <?php } ?>
                                
							<?php $r=$universal_product_row['product_buy_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $universal_product_row['product_buy_url']; ?>" target="_blank"><img src="images/cart.svg" alt=""/></a></span>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="product-desc">
                            <h4 class="product-title"><?php echo $universal_product_row['product_name']; ?></h4>
                            
                        </div>
                    <?php } ?>
                </div>
                <div class="product">
                	<?php
				   	$treat_query = "SELECT * FROM product_data WHERE product_name = '". $treat_higher_value_key ."'";
					$treat_result = mysqli_query($connection, $treat_query) or die(mysqli_error($connection)); 
					while ($treat_row = mysqli_fetch_array($treat_result, MYSQLI_BOTH)) { ?>
                        <div class="product-image">
                        	<span class="product-step">Step 3: Treat</span>
                            <div class="product-share">
                                
                                <span class="icons">
                                    <a href="https://www.facebook.com/elizabethardensouthafrica/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                     <a href="https://twitter.com/EArdenSA" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.instagram.com/elizabetharden/" target="_blank"><i class="fab fa-instagram"></i></a>
                                </span>
                            </div>
                            <img class="quiz-image" data-src="<?php echo $_SESSION['base_url'].'product-images/'. $treat_row['product_image']. ''; ?>" alt="<?php echo $treat_row['product_name']; ?>">
                            <div class="product-info">
                            
							<?php $r=$treat_row['product_info_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $treat_row['product_info_url']; ?>" target="_blank"><img src="images/info.svg" alt=""/></a></span>
                            <?php } ?>
                                
							<?php $r=$treat_row['product_buy_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $treat_row['product_buy_url']; ?>" target="_blank"><img src="images/cart.svg" alt=""/></a></span>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="product-desc">
                            <h4 class="product-title"><?php echo $treat_row['product_name']; ?></h4>
                            
                        </div>
                    <?php } ?>
                </div>
                <div class="product">
                	<?php
				   	$moisturise_query = "SELECT * FROM product_data WHERE product_name = '". $moisturise_higher_value_key ."'";
					$moisturise_result = mysqli_query($connection, $moisturise_query) or die(mysqli_error($connection)); 
					while ($moisturise_row = mysqli_fetch_array($moisturise_result, MYSQLI_BOTH)) { ?>
                        <div class="product-image">
                        	<span class="product-step">Step 4: Moisturise</span>
                            <div class="product-share">
                                
                                <span class="icons">
                                    <a href="https://www.facebook.com/elizabethardensouthafrica/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                     <a href="https://twitter.com/EArdenSA" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.instagram.com/elizabetharden/" target="_blank"><i class="fab fa-instagram"></i></a>
                                </span>
                            </div>
                            <img class="quiz-image" data-src="<?php echo $_SESSION['base_url'].'product-images/'. $moisturise_row['product_image']. ''; ?>" alt="<?php echo $moisturise_row['product_name']; ?>">
                            <div class="product-info">
                            
							<?php $r=$moisturise_row['product_info_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $moisturise_row['product_info_url']; ?>" target="_blank"><img src="images/info.svg" alt=""/></a></span>
                            <?php } ?>
                                
							<?php $r=$moisturise_row['product_buy_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $moisturise_row['product_buy_url']; ?>" target="_blank"><img src="images/cart.svg" alt=""/></a></span>
                            <?php } ?>
                                
                            </div>
                        </div>
                        <div class="product-desc">
                            <h4 class="product-title"><?php echo $moisturise_row['product_name']; ?></h4>
                            
                        </div>
                    <?php } ?>
                </div>
                </div>
                <div class="ymltp-block">
                <h3 class="small-heading">You may also love these</h3>
              <div class="product-ymltp">
                	<?php
				   	$ymltp_query = "SELECT * FROM you_may_love_these_products";
					$ymltp_result = mysqli_query($connection, $ymltp_query) or die(mysqli_error($connection)); 
					while ($ymltp_row = mysqli_fetch_array($ymltp_result, MYSQLI_BOTH)) { ?>
                    <div class="product">
						<div class="product-image">
                            <img class="quiz-image" data-src="<?php echo $_SESSION['base_url'].'product-images/'. $ymltp_row['product_image']. ''; ?>" alt="<?php echo $ymltp_row['product_name']; ?>">
							
                        </div>
                        <div class="product-desc">
                            <h4 class="product-title"><?php echo $ymltp_row['product_name']; ?></h4>
							
                        </div>
						<div class="product-info">
							<?php $r=$ymltp_row['product_info_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $ymltp_row['product_info_url']; ?>" target="_blank"><img src="images/info.svg" alt="" class="mCS_img_loaded"></a></span>
                            <?php } ?>                               
							
							<?php $r=$ymltp_row['product_buy_url']; if($r==null or $r=="" or $r==" "){ ?><span class="info-link"> </span>
                            <?php }else{ ?><span class="info-link"><a href="<?php echo $ymltp_row['product_buy_url']; ?>" target="_blank"><img src="images/cart.svg" alt="" class="mCS_img_loaded"></a></span>
                            <?php } ?>
                                
						</div>
                    </div>
                    <?php } ?>
                </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<footer class="footer color-1">
  <p class="copyright">© 2021 Elizabeth Arden, Inc. All Rights Reserved</p>
</footer>
<?php include 'email/send_email_result.php'; ?>
<?php } ?>
<?php mysqli_close($connection); ?>
<?php //session_destroy(); ?>