-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: mysql1004.mochahost.com
-- Generation Time: Dec 30, 2019 at 05:46 AM
-- Server version: 5.6.33
-- PHP Version: 7.2.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rwshs007_dncampaign`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` longtext NOT NULL,
  `combination_answers` longtext NOT NULL,
  `product_name` longtext NOT NULL,
  `category_name` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=162 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `question`, `combination_answers`, `product_name`, `category_name`) VALUES
(1, 'skin_care', 'H,J', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(2, 'birthdays_celebrated', 'A', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(3, 'skin_feel', 'A,B,C,D', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(4, 'describe_diet', 'B,C', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(5, 'spend_outside', 'A,B', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(6, 'savvy_suncare', 'A,B,C,D', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(7, 'stress_levels', 'A,B', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'CLEANSER'),
(8, 'skin_care', 'A,C,I,K', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(9, 'birthdays_celebrated', 'B,C,D,E', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(10, 'skin_feel', 'A,B,C', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(11, 'describe_diet', 'B,C', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(12, 'spend_outside', 'A,B', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(13, 'savvy_suncare', 'A,B,C,D', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(14, 'stress_levels', 'A,B', 'Ceramide Purifying Cream Cleanser 125ml', 'CLEANSER'),
(15, 'skin_care', 'A,C,I,K', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(16, 'birthdays_celebrated', 'B,C,D,E', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(17, 'skin_feel', 'A,B,C', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(18, 'describe_diet', 'B,C', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(19, 'spend_outside', 'A,B ', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(20, 'savvy_suncare', 'A,B,C,D', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(21, 'stress_levels', 'A,B', 'Ceramide Replenishing Cleansing Oil 195ml', 'CLEANSER'),
(22, 'skin_care', 'B,D,E,F,G,K', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(23, 'birthdays_celebrated', 'A,B,C,D,E', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(24, 'skin_feel', 'A,B,C,D', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(25, 'describe_diet', 'A ', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(26, 'spend_outside', 'C,D', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(27, 'savvy_suncare', 'B,C,D', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(28, 'stress_levels', 'C,D', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'CLEANSER'),
(29, 'skin_care', 'F,H', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(30, 'birthdays_celebrated', 'A,B,C,D,E', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(31, 'skin_feel', 'C,D', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(32, 'describe_diet', 'B,C', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(33, 'spend_outside', 'A,B', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(34, 'savvy_suncare', 'A,B,C,D', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(35, 'stress_levels', 'A,B', 'Skin Illuminating Smoothing Cleanser 125ml', 'CLEANSER'),
(36, 'skin_care', 'C,J', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(37, 'birthdays_celebrated', 'A,B,C,D,E', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(38, 'skin_feel', 'A,B,C,D', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(39, 'describe_diet', 'A,B,C', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(40, 'spend_outside', 'A,B,C,D', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(41, 'savvy_suncare', 'A,B,C,D', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(42, 'stress_levels', 'A,B', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'CLEANSER'),
(43, 'skin_care', 'A,B,C,D,E,F,G,H,I,J,K', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(44, 'birthdays_celebrated', 'A,B,C,D,E', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(45, 'skin_feel', 'A,B,C,D', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(46, 'describe_diet', 'A,B,C', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(47, 'spend_outside', 'A,B,C,D', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(48, 'savvy_suncare', 'A,B,C,D', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(49, 'stress_levels', 'A,B,C,D', 'Superstart Skin Renewal Booster 50ml', 'UNIVERSAL PRODUCT'),
(50, 'skin_care', 'A,C,I,K', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(51, 'birthdays_celebrated', 'B,C,D,E', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(52, 'skin_feel', 'A,B,C,D', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(53, 'describe_diet', 'B,C ', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(54, 'spend_outside', 'A,B', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(55, 'savvy_suncare', 'A,B,C,D', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(56, 'stress_levels', 'A,B', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'TREAT'),
(57, 'skin_care', 'I,K', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(58, 'birthdays_celebrated', 'B,C,D', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(59, 'skin_feel', 'A,B,C,D', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(60, 'describe_diet', 'B,C ', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(61, 'spend_outside', 'A,B', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(62, 'savvy_suncare', 'A,B,C,D', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(63, 'stress_levels', 'A,B', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'TREAT'),
(64, 'skin_care', 'B,E,F,G,K', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(65, 'birthdays_celebrated', 'A,B,C,D,E', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(66, 'skin_feel', 'A,B,C,D', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(67, 'describe_diet', 'A,B', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(68, 'spend_outside', 'D', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(69, 'savvy_suncare', 'A,B,C,D', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(70, 'stress_levels', 'C,D', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'TREAT'),
(71, 'skin_care', 'B,F,G,K', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(72, 'birthdays_celebrated', 'A,B,C,D,E', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(73, 'skin_feel', 'A,B,C,D', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(74, 'describe_diet', 'A,B', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(75, 'spend_outside', 'C ', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(76, 'savvy_suncare', 'A,B,C,D', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(77, 'stress_levels', 'C,D', 'Prevage Anti-Aging Daily Serum 50ml', 'TREAT'),
(78, 'skin_care', 'B,G,H,I', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(79, 'birthdays_celebrated', 'A,B,C,D,E', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(80, 'skin_feel', 'C,D', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(81, 'describe_diet', 'A,B', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(82, 'spend_outside', 'A,B,C,D', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(83, 'savvy_suncare', 'A,B,C,D', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(84, 'stress_levels', 'A,B,C,D', 'Prevage City Smart + DNA Enzyme Complex', 'TREAT'),
(85, 'skin_care', 'F,H,K', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(86, 'birthdays_celebrated', 'A,B,C,D,E', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(87, 'skin_feel', 'C,D', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(88, 'describe_diet', 'B,C ', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(89, 'spend_outside', 'A,B,C,D', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(90, 'savvy_suncare', 'A,B,C,D', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(91, 'stress_levels', 'A,B', 'Skin Illuminating Brightening Day Serum 30ml', 'TREAT'),
(92, 'skin_care', 'C,D,J', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(93, 'birthdays_celebrated', 'A,B,C,D,E', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(94, 'skin_feel', 'A,B,C,D', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(95, 'describe_diet', 'B,C', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(96, 'spend_outside', 'A,B,C,D', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(97, 'savvy_suncare', 'A,B,C,D', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(98, 'stress_levels', 'A,B', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'TREAT'),
(99, 'skin_care', 'F,I,K', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(100, 'birthdays_celebrated', 'A,B,C,D,E', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(101, 'skin_feel', 'A,B,C,D', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(102, 'describe_diet', 'B,C', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(103, 'spend_outside', 'A,B,C,D', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(104, 'savvy_suncare', 'A,B,C,D', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(105, 'stress_levels', 'A,B', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', 'TREAT'),
(106, 'skin_care', 'F,H,I,K', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(107, 'birthdays_celebrated', 'A,B,C,D,E', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(108, 'skin_feel', 'C,D', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(109, 'describe_diet', 'B,C ', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(110, 'spend_outside', 'A,B,C,D', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(111, 'savvy_suncare', 'A,B,C,D', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(112, 'stress_levels', 'A,B', 'Skin Illuminating Brightening Night Capsules 50pc', 'TREAT'),
(113, 'skin_care', 'C,D,J', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(114, 'birthdays_celebrated', 'A,B', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(115, 'skin_feel', 'A,B', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(116, 'describe_diet', 'B,C', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(117, 'spend_outside', 'A,B', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(118, 'savvy_suncare', 'A,B,C,D', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(119, 'stress_levels', 'A,B', 'Visible Difference Refining Moisture Cream Complex 75ml', 'MOISTURISE'),
(120, 'skin_care', 'C,I,K', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(121, 'birthdays_celebrated', 'B,C', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(122, 'skin_feel', 'A,B,C,D', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(123, 'describe_diet', 'B,C', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(124, 'spend_outside', 'A,B', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(125, 'savvy_suncare', 'A,B,C,D', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(126, 'stress_levels', 'A,B,C', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'MOISTURISE'),
(127, 'skin_care', 'A,C,I,K', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(128, 'birthdays_celebrated', 'C,D', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(129, 'skin_feel', 'A,B,C,D', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(130, 'describe_diet', 'B,C', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(131, 'spend_outside', 'A,B', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(132, 'savvy_suncare', 'A,B,C,D', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(133, 'stress_levels', 'A,B', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'MOISTURISE'),
(134, 'skin_care', 'C,D,I,K', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(135, 'birthdays_celebrated', 'D,E', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(136, 'skin_feel', 'A,B,C', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(137, 'describe_diet', 'B,C', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(138, 'savvy_suncare', 'A,B ', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(139, 'spend_outside', 'A,B,C,D', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(140, 'stress_levels', 'A,B', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(141, 'skin_care', 'B,F,G,I,K', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(142, 'birthdays_celebrated', 'A,B,C,D,E', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(143, 'skin_feel', 'A,B,C,D', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(144, 'describe_diet', 'A,B', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(145, 'spend_outside', 'C,D ', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(146, 'savvy_suncare', 'A,B,C,D', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(147, 'stress_levels', 'C,D ', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'MOISTURISE'),
(148, 'skin_care', 'F,H,K', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(149, 'birthdays_celebrated', 'A,B,C,D,E', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(150, 'skin_feel', 'C,D ', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(151, 'describe_diet', 'B,C', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(152, 'spend_outside', 'A,B', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(153, 'savvy_suncare', 'A,B,C,D', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(154, 'stress_levels', 'A,B', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'MOISTURISE'),
(155, 'skin_care', 'B,C,D,J', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE'),
(156, 'birthdays_celebrated', 'A,B,C,D,E', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE'),
(157, 'skin_feel', 'A,B,C,D', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE'),
(158, 'describe_diet', 'A,B,C', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE'),
(159, 'spend_outside', 'A,B,C,D', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE'),
(160, 'savvy_suncare', 'A,B,C,D', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE'),
(161, 'stress_levels', 'A,B', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', 'MOISTURISE');

-- --------------------------------------------------------

--
-- Table structure for table `product_data`
--

CREATE TABLE IF NOT EXISTS `product_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` longtext NOT NULL,
  `product_name` longtext NOT NULL,
  `product_buy_url` longtext NOT NULL,
  `product_image` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `product_data`
--

INSERT INTO `product_data` (`id`, `category_name`, `product_name`, `product_buy_url`, `product_image`) VALUES
(1, 'CLEANSER', 'Visible Difference Skin Balancing Exfoliating Cleanser 125ml', 'https://www.elizabetharden.co.za/product/64/Visible-Difference-Skin-Balancing-Exfoliating-Cleanser/', 'i-1.jpg'),
(2, 'CLEANSER', 'Ceramide Purifying Cream Cleanser 125ml', 'https://www.elizabetharden.co.za/product/11/Ceramide-Purifying-Cream-Cleanser/', 'i-2.jpg'),
(3, 'CLEANSER', 'Ceramide Replenishing Cleansing Oil 195ml', 'https://www.elizabetharden.co.za/product/141/Ceramide-Replenishing-Cleansing-Oil/', 'i-3.jpg'),
(4, 'CLEANSER', 'Prevage Anti-Aging Treatment Boosting Cleanser 125ml', 'https://www.elizabetharden.co.za/product/112/PREVAGE-Anti-aging-Treatment-Boosting-Cleanser/', 'i-4.jpg'),
(5, 'CLEANSER', 'Skin Illuminating Smoothing Cleanser 125ml', 'https://www.elizabetharden.co.za/product/101/Skin-Illuminating-Smoothing-Cleanser/', 'i-5.jpg'),
(6, 'CLEANSER', 'Superstart Probiotic Cleanser Whip to Clay 125ml', 'https://www.elizabetharden.co.za/product/191/SUPERSTART-Probiotic-Cleanser-Whip-to-Clay/', 'i-6.jpg'),
(7, 'UNIVERSAL PRODUCT', 'Superstart Skin Renewal Booster 50ml', 'https://www.elizabetharden.co.za/product/153/SUPERSTART-Skin-Renewal-Booster/', 'i-7.jpg'),
(8, 'TREAT', 'Advanced Ceramide Capsules Daily Youth Restoring Serum 60pc', 'https://www.elizabetharden.co.za/product/167/Advanced-Ceramide-Capsules-Daily-Youth-Restoring-Serum-60-Piece/', 'i-8.jpg'),
(9, 'TREAT', 'Retinol Ceramide Capsules Line Erasing Night Serum 60pc', 'https://www.elizabetharden.co.za/product/198/Retinol-Ceramide-Capsules-Line-Erasing-Night-Serum-60-Piece/', 'i-9.jpg'),
(10, 'TREAT', 'Prevage Anti-Aging + Intensive Repair Daily Serum 30ml', 'https://www.elizabetharden.co.za/product/95/PREVAGE-Anti-Aging-Intensive-Repair-Daily-Serum/', 'i-10.jpg'),
(11, 'TREAT', 'Prevage Anti-Aging Daily Serum 50ml', 'https://www.elizabetharden.co.za/product/16/PREVAGE-Anti-aging-Daily-Serum/', 'i-11.jpg'),
(12, 'TREAT', 'Prevage City Smart + DNA Enzyme Complex', '', 'i-12.jpg'),
(13, 'TREAT', 'Skin Illuminating Brightening Day Serum 30ml', 'https://www.elizabetharden.co.za/product/142/Skin-Illuminating-Brightening-Day-Serum-With-Advanced-MIX-ConcentrateTM/', 'i-13.jpg'),
(14, 'TREAT', 'Eight Hour Cream Sun Defense for Face SPF50 PA+++ 50ml', 'https://www.elizabetharden.co.za/product/24/Eight-Hour-Cream-Sun-Defense-for-Face-SPF-50-Sunscreen-High-Protection-PA/', 'i-14.jpg'),
(15, 'TREAT', 'Vitamin C Ceramide Capsules Radiance Renewal Serum 60 pc', '', 'i-15.jpg'),
(16, 'TREAT', 'Skin Illuminating Brightening Night Capsules 50pc', 'https://www.elizabetharden.co.za/product/143/Skin-Illuminating-Brightening-Night-Capsules-With-Advanced-MIX-ConcentrateTM/', 'i-16.jpg'),
(17, 'MOISTURISE', 'Visible Difference Refining Moisture Cream Complex 75ml', 'https://www.elizabetharden.co.za/product/176/Visible-Difference-Refining-Moisture-Cream-Complex/', 'i-17.jpg'),
(18, 'MOISTURISE', 'Flawless Future Powered by Ceramide Moisture Cream SPF30 PA++50ml', 'https://www.elizabetharden.co.za/product/132/FLAWLESS-FUTURE-Powered-by-CeramideTM-Moisture-Cream-SPF-30-PA/', 'i-18.jpg'),
(19, 'MOISTURISE', 'Ceramide Lift and Firm Day Cream SFP30 PA++ 50ml', 'https://www.elizabetharden.co.za/product/1/Ceramide-Lift-and-Firm-Day-Cream-SPF-30-PA/', 'i-19.jpg'),
(20, 'MOISTURISE', 'Ceramide Premiere Intense Moisture and Renewal Activation Cream SPF30 PA++ 50ml', 'https://www.elizabetharden.co.za/product/87/Ceramide-Premiere-Intense-Moisture-and-Renewal-Activation-Cream-SPF-30-PA/', 'i-20.jpg'),
(21, 'MOISTURISE', 'Prevage Anti-Aging Moisture Cream SPF30 PA++ 50ml', 'https://www.elizabetharden.co.za/product/83/PREVAGE-Anti-aging-Moisture-Cream-SPF-30-PA/', 'i-21.jpg'),
(22, 'MOISTURISE', 'Skin Illuminating Brightening Hydragel Cream 50ml', 'https://www.elizabetharden.co.za/product/181/Skin-Illuminating-Brightening-Hydragel-Cream/', 'i-22.jpg'),
(23, 'MOISTURISE', 'Eight Hour Great 8 Daily Defense Moisturizer Broad Spectrum Sunscreen SPF35', '', 'i-23.jpg'),
(24, '', 'Eight Hour Cream Skin Protectant 50ml', '', 'i-24.jpg'),
(25, '', 'Advanced Ceramide Capsules Daily Youth Restoring Eye Serum 60pc', '', 'i-25.jpg'),
(26, '', 'Flawless Start Instant Perfecting Primer 30ml', '', 'i-26.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_quizzes`
--

CREATE TABLE IF NOT EXISTS `user_quizzes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identity_number` int(11) NOT NULL,
  `favourite_skin_care` longtext NOT NULL,
  `skin_care` longtext NOT NULL,
  `birthdays_celebrated` longtext NOT NULL,
  `skin_feel` longtext NOT NULL,
  `describe_diet` longtext NOT NULL,
  `spend_outside` longtext NOT NULL,
  `savvy_suncare` longtext NOT NULL,
  `stress_levels` longtext NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birth_date` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `age_range` varchar(255) NOT NULL,
  `skin_ethnicity` varchar(255) NOT NULL,
  `receive_promotional_material` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `you_may_love_these_products`
--

CREATE TABLE IF NOT EXISTS `you_may_love_these_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` longtext NOT NULL,
  `product_image` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `you_may_love_these_products`
--

INSERT INTO `you_may_love_these_products` (`id`, `product_name`, `product_image`) VALUES
(1, 'Eight Hour Cream Skin Protectant 50ml', 'i-24.jpg'),
(2, 'Advanced Ceramide Capsules Daily Youth Restoring Eye Serum 60pc', 'i-25.jpg'),
(3, 'Flawless Start Instant Perfecting Primer 30ml', 'i-26.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
